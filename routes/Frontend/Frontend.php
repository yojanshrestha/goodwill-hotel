<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', 'FrontendController@index')->name('index');

Route::get('/{slug}', 'FrontendController@page')->name('page');

Route::get('/accomodation/{slug}', 'FrontendController@accomodationDetail')->name('accomodationDetail');

Route::get('/facility/{slug}', 'FrontendController@facilitiesDetail')->name('facilityDetail');

Route::post('/sendemail', 'FrontendController@sendEmail')->name('sendemail');

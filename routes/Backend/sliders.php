<?php

	Route::group([
		'middleware'=> 'access.routeNeedsPermission:view-slider-management'
		],function(){

			Route::resource('sliders','SliderController');
			Route::patch('sliders/{title}',"SliderController@update");
			Route::get('sliders/{title}/list','SliderController@slide_list')->name('admin.sliders.slide_list');
			Route::get('sliders/{slider_title}/create','SliderController@slide_create');
			Route::patch('slide/{id}','SliderController@updateSlide');	

			Route::get('slides/{id}/edit','SliderController@edit')->name('slides.edit');
    		Route::delete('/slides/destroy/{id}','SliderController@slide_delete')->name('slides.destroy'); 

    		Route::get('/data/table/sliders','SliderController@load');
		    Route::post('/data/table/slide/list','SliderController@load_slide_list');


		});
<?php
	
	Route::group([
		'middleware' => 'access.routeNeedsPermission:view-blocks-management'
		],function(){
			Route::get('/blocks', 'BlockController@create')->name('blocks');
			Route::post('/blocks', 'BlockController@store')->name('blocks.store');
			Route::patch('/blocks', 'BlockController@store')->name('blocks.update');
	});
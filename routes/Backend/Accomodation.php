<?php
/*
* Accomodations Management
*/
Route::group([
	'middleware' => 'access.routeNeedsPermission:view-accomodations-management'
], function() {
	
	Route::delete('accomodations/bulkdelete', 'AccomodationController@deleteAccomodations');
    Route::resource('accomodations', 'AccomodationController', ['except' => ['show']]);

	Route::get('/data/table/accomodations', 'AccomodationController@load');
});
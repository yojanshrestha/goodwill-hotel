<?php
/*
* Testimonials Management
*/
Route::group([
	'middleware' => 'access.routeNeedsPermission:view-testimonials-management'
], function() {
	
	Route::delete('testimonials/bulkdelete', 'TestimonialController@deleteTestimonials');
    Route::resource('testimonials', 'TestimonialController', ['except' => ['show']]);

	Route::get('/data/table/testimonials', 'TestimonialController@load');
});
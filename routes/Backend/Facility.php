<?php
/*
* Facilities Management
*/
Route::group([
	'middleware' => 'access.routeNeedsPermission:view-facilities-management'
], function() {
	
	Route::delete('facilities/bulkdelete', 'FacilityController@deleteFacilities');
    Route::resource('facilities', 'FacilityController', ['except' => ['show']]);

	Route::get('/data/table/facilities', 'FacilityController@load');
});
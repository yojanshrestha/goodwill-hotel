function carousel() {
    $('.slider .owl-carousel').owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: true,
        autoplay: true,

        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
    })
    $('.testimonials .owl-carousel').owlCarousel({
        items: 1,
        nav: true,
        dots: false,
        loop: true,
        autoplay: true
    })
    $('.feature-slider .owl-carousel').owlCarousel({
        items: 1,
        nav: false,
        dots: false,
        loop: true,
        autoplay: true
    })
    $('.other-facilities .owl-carousel').owlCarousel({
        items: 5,
        nav: false,
        dots: false,
        loop: true,
        autoplay: true,
        margin: 10
    })
}

function lightCase() {
    $('a[data-rel^=lightcase]').lightcase();
}

function scrollTopLoad() {
    if($('.onload-top').length > 0){
        $('html,body').animate({
            scrollTop: $('.onload-top').offset().top
        }, 1500);
    }
}

function stickyTop() {
    var sticky = new Waypoint.Sticky({
        element: $('.navigation')[0]
    })
}
$(document).ready(function() {
    carousel();
    lightCase();
    scrollTopLoad();
    stickyTop();

    $('#contactform').validate();

    $('#contactform').on('submit',function(){
        $('.loaderimg').show()
        if($(this).valid() ){
            var values = $(this).serialize()
                $.ajax({
                url: base_url+'/sendemail',
                    data: { values : values},
                     headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: "json",
                    type: "post",
                    success: function(data) { 
                    $('.loaderimg').hide()
                    $('.success').html('Your email has been send. We will contact you shortly.').fadeOut(5000)
                    $('form#contactform')[0].reset()
                    },
                    error: function(){
                    $('.success').html('Something Went Wrong! Please try again.').fadeOut(5000)
                    $('.loaderimg').hide()
                    }
        })
        }
    })
})

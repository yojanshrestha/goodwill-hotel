$(document).ready(function(){

  $.validator.setDefaults({
    errorElement: "span",
    errorClass: "help-block",
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function (error, element) {
     
    }
  });

  $(function() { $('#cp8').colorpicker({ colorSelectors: { 'black': '#000000', 'white': '#ffffff', 'red': '#FF0000', 'default': '#777777', 'primary': '#337ab7', 'success': '#5cb85c', 'info': '#5bc0de', 'warning': '#f0ad4e', 'danger': '#d9534f' } }); });

  /** --select all -- **/
  $(document).on('change', '.checkAll', function(){
    $(".bulkSelect").prop('checked', $(this).prop("checked"));
    
  });

  /** ----- for bulk deleting ---- **/
  $(document).on('change', '.bulkSelect', function(){
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(false == $(this).prop("checked")){ //if this item is unchecked
          $(".checkAll").prop('checked', false); //change "select all" checked status to false
      }
     //check "select all" if all checkbox items are checked
    if ($('.bulkSelect:checked').length == $('.bulkSelect').length ){
        $(".checkAll").prop('checked', true);
    }
  });


  /**  Page table      **/
  $('#page-table').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "ajax": base_url + "/admin/data/table/pages",
    columns: [
    {data: 'bulk', name: 'bulk', orderable: false, searchable: false},
    {data: 'id', name: 'id'},
    {data: 'title', name: 'title'},
    {data: 'slug', name: 'slug'},
    {data: 'footer_menu', name: 'footer_menu'},
    {data: 'created_at', name: 'created_at'},
    {data: 'updated_at', name: 'updated_at'},
    {data: 'status', name: 'status'},
    {data: 'action', name: 'action', orderable: false, searchable: false},
    ],
    order: [[1, "asc"]]
  });

  /**  Testimonial table      **/
  $('#testimonial-table').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "ajax": base_url + "/admin/data/table/testimonials",
    columns: [
    {data: 'bulk', name: 'bulk', orderable: false, searchable: false},
    {data: 'id', name: 'id'},
    {data: 'name', name: 'name'},
    {data: 'address', name: 'address'},
    {data: 'ordering', name: 'ordering'},
    {data: 'created_at', name: 'created_at'},
    {data: 'updated_at', name: 'updated_at'},
    {data: 'status', name: 'status'},
    {data: 'action', name: 'action', orderable: false, searchable: false},
    ],
    order: [[4, "asc"]]
  });

  //general function for submiting form using <a> tag
  $(document).on('click', 'a.bulkSubmit', function(){
    var link = $(this);
    var tmp = $(this);
    var cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : "Cancel";
    var confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : "Yes, delete";
    var title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : "Are you sure you want to delete?";
    var text = (link.attr('data-trans-text')) ? link.attr('data-trans-text') : "Are you sure you want to delete?";

    swal({
      title: title,
      type: "warning",
      showCancelButton: true,
      cancelButtonText: cancel,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: confirm,
      closeOnConfirm: true
    }, function(confirmed) {
      if (confirmed){
        var bulkChecked = $('.bulkSelect:checked').map(function() {
          return $(this).attr('data-id');
        }).get();
        $('.ids').val(bulkChecked);
      // console.log(bulkChecked);
        link.closest('form').submit();
      }
    });
  });

  /**  Facilities Management && Accomodation Management   **/
  $('#facility-table').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "ajax": base_url + "/admin/data/table/facilities",
    columns: [
    {data: 'bulk', name: 'bulk', orderable: false, searchable: false},
    {data: 'id', name: 'id'},
    {data: 'title', name: 'title'},
    {data: 'slug', name: 'slug'},
    {data: 'featured_facility', name: 'featured_facility'},
    {data: 'ordering', name: 'ordering'},
    // {data: 'accomodation_only', name: 'accomodation_only'},
    {data: 'detail_page', name: 'detail_page'},
    {data: 'created_at', name: 'created_at'},
    {data: 'updated_at', name: 'updated_at'},
    {data: 'status', name: 'status'},
    {data: 'action', name: 'action', orderable: false, searchable: false},
    ],
    order: [[5, "asc"]]
  });

  $('#accomodation-table').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "ajax": base_url + "/admin/data/table/accomodations",
    columns: [
    {data: 'bulk', name: 'bulk', orderable: false, searchable: false},
    {data: 'id', name: 'id'},
    {data: 'title', name: 'title'},
    {data: 'slug', name: 'slug'},
    {data: 'ordering', name: 'ordering'},
    {data: 'created_at', name: 'created_at'},
    {data: 'updated_at', name: 'updated_at'},
    {data: 'status', name: 'status'},
    {data: 'action', name: 'action', orderable: false, searchable: false},
    ],
    order: [[4, "asc"]]
  });

  deleteBtnShowHide();
  $(document).on('click', '.btn-group .btn-add', function(){
    $html = $('.gallery-extra').html();
    $('.gallery-wrapper').append($html);
    deleteBtnShowHide();
  });

  $(document).on('click', '.btn-remove', function(){
    $elem = $(this).closest('.single-gallery');
    $elem.slideUp('normal', function(){
      $elem.remove();
      deleteBtnShowHide();
    });
  });

  $('#facilityForm').validate();
  $('#accomodationForm').validate();

  $(document).on('click', '.btn-group .feature-btn-add', function(){
    $html = $('.feature-extra').html();
    $('.feature-wrapper').append($html);
    deleteBtnShowHide();
  });

  $(document).on('click', '.feature-btn-remove', function(){
    $elem = $(this).closest('.single-feature');
    $elem.slideUp('normal', function(){
      $elem.remove();
      deleteBtnShowHide();
    });
  });
  /** End- Facilities Management && Accomodation Management  **/


  /**  Block Management  **/
  $('#blockForm').validate();
  
  $(document).on('click', '#blockForm .btn-add', function(){
    $html = $('.block-extra').html();
    $('.block-wrapper').append($html);
  });

  $(document).on('click', '#blockForm .btn-remove', function(){
    $elem = $(this).closest('.single-block');
    $elem.slideUp('normal', function(){
      $elem.remove();
    });
  });
  /**  End- Block Management  **/

});


/**
 * method used to show image preview when image is selected
 */
function readURL(input, preview, $parent = '#feat-img-preview') {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $($parent).hide();
      $(preview).css('background-image', 'url('+e.target.result+')').show();
    }

    reader.readAsDataURL(input.files[0]);
  }else{
    $(preview).hide().css('background-image', 'url(\'\')');
  }
}

/**
 * method used to show image preview when image is selected
 */
function readImageURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
    var tmp = $(input).closest('.form-group').find('.show-img-bg').first();
      $(tmp).css('background-image', 'url('+e.target.result+')').show();
      $(input).closest('.form-group').find('.image-preview').hide();
    }
    reader.readAsDataURL(input.files[0]);
  }else{
    var tmp = $(input).closest('.form-group').find('.show-img-bg').first();
    $(tmp).hide().css('background-image', 'url(\'\')');
  }
}


/**
   * Check File Types.
*/

function Validate(input)
{
    var ext = $(input).val().split('.').pop().toLowerCase();
   
    if($.inArray(ext, ['gif','png','jpg','jpeg','mp4']) == -1) {
       return false;
    }
    else{

      return true;
    }    
}

 // Give image source to preview with preview element as argument.

function getImageSrc(input,preview){

  if(Validate(input)===false){

    swal({                  
      title: "Invalid Image Type!",
      type:"error", 
      text: "Valid extensions are gif, png, jpg, jpeg",
      timer: 1000,
      showConfirmButton: false             
    });

    $(input).val('');
    $(preview).hide();
    return false;
  }

  if(input.files && input.files[0]){

    var reader = new FileReader();
    
    reader.onload = function(e){
      preview.attr('src',e.target.result);
      // $(preview).css('background-image', 'url('+e.target.result+')').show();
    }

    reader.readAsDataURL(input.files[0]);
  }
}

function elFinderBrowser(callback, value, meta) {
        var request = base_url + 'yelfinder/tinymce4';
        tinymce.activeEditor.windowManager.open({
            title: 'admin.elfinder',
            url: request,
            width: 900,
            height: 450,
           path : '/public/files/',
            resizable: 'yes',
            
            uiOptions: {
                    toolbar : [
                        // toolbar configuration
                        ['open'],
                        ['back', 'forward'],
                        ['reload'],
                        ['home', 'up'],
                        ['mkdir', 'mkfile', 'upload'],
                        ['info'],
                        ['quicklook'],
                        ['copy', 'cut', 'paste'],
                        ['rm'],
                        ['duplicate', 'rename', 'edit'],
                        ['extract', 'archive'],
                        ['search'],
                        ['view'],
                        ['help']
                    ],
                    path : '/public/files/'
                },
                contextmenu : {
                    files  : [
                        'getfile', '|','open', '|', 'copy', 'cut', 'paste', 'duplicate', '|',
                        'rm', '|', 'edit', 'rename', '|', 'archive', 'extract', '|', 'info'
                    ]
                }
        }, {
            setUrl: function (url) {
                callback(url);
                //$('.mce-textbox').val(url.replace('files','public/files'));
               
            }
        });
        return false;
    }

  function readLogoURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#previewLogo').attr('src', e.target.result).removeClass('hide');
      $("#img-preview").hide();
    }
    reader.readAsDataURL(input.files[0]);
  }else{
    $('#previewLogo').addClass('hide').attr('src', 'url(\'\')');
  }
}

function readFaviconURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#previewFavicon').attr('src', e.target.result).removeClass('hide');
      $("#img-preview2").hide();
    }
    reader.readAsDataURL(input.files[0]);
  }else{
    $('#previewFavicon').addClass('hide').attr('src', 'url(\'\')');
  }
}

/** slider delete btn show/hide for facilities and accomodation **/
function deleteBtnShowHide(){
  if ($('.gallery-wrapper .single-gallery').length > 1) {
    $('.gallery-wrapper .btn-remove').removeClass('hide');
  }else{
    $('.gallery-wrapper .btn-remove').addClass('hide');
  }

  if ($('.feature-wrapper .single-feature').length > 0) {
    $('.feature-wrapper .feature-btn-remove').removeClass('hide');
  }else{
    $('.feature-wrapper .feature-btn-remove').addClass('hide');
  }
}

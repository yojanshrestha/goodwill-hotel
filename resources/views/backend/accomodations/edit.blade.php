@extends('backend.layouts.app')

@section ('title', 'Edit Accomodation')

@section('page-header')
<h1>
	Edit Accomodation
</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Facilities</a></li>
		<li class="active">
			Edit Accomodation
		</li>
	</ol>
@endsection

@section('content')
<!-- Main content -->

	<div class="gallery-extra hide">
		<div class="single-gallery">
			<div class="form-group">
				<label class="control-label">Ordering<em class="asterisk">*</em></label>
				{{Form::number('gallery_ordering[]', null, ['class'=>'form-control', 'placeholder'=>'Enter Ordering', 'step'=>1, 'required', 'min'=>0])}}
			</div>
			
			{{Form::hidden('prev_image[]', null)}}
			<div class="form-group">
				<label class="control-label">Image<em class="asterisk">*</em></label>
				<div class="">
					<span class="btn btn-sm btn-karm btn-file ">
						<i class="fa fa-folder-open"></i> Upload Image
						<input type="file" name="gallery[]" accept="image/*" class="image-upload" onchange="readImageURL(this);" required>
					</span>
					<div class="show-img-bg mt-10 display-none" alt="Image Preview"></div>
				</div>
				<div class="mt-10">
					<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-remove"><i class="fa fa-trash"></i> Delete</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		{{Form::model($accomodation,['url'=>'admin/accomodations/'.$accomodation->id, 'files'=> 'true', 'id'=>'accomodationForm', 'method'=>'patch'])}}
		<div class="col-md-9">
			<div class="box box-orange">
				<div class="box-header">
					<!-- <h3 class="box-title">Create Accomodation</h3> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="form-group">
						<label class="control-label">Title<em class="asterisk">*</em></label>
						{{Form::text('title',null,['class'=>'form-control', 'placeholder'=>'Enter Title', 'required'])}}
					</div>

					<div class="form-group">
						<label class="control-label">Slug</label>
						<div class="input-group">
						  <span class="input-group-addon">{{url('/').'/accomodation/'}}</span>
						  {{Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'Enter Slug'])}}
						</div>
						
					</div>

					<div class="form-group">
						<label class="control-label">Content</label>
							{!!Form::textarea('content',null,['class'=>'form-control', 'placeholder'=>'Enter Content']) !!}
					</div>

				</div>
				<!-- /.box-body -->
			</div>

			<div class="box box-orange">
				<div class="box-header with-border">
					<h3 class="box-title">Facilities</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<div class="feature-wrapper normal-text">
						@if(count($facilities) > 0)
						<div class="row">
							@foreach($facilities as $facility)
							<div class="col-md-4"> 
								@if(in_array($facility->id, $selectedFacilities))
									{{Form::checkbox('facility_id[]', $facility->id, true, ['id' => 'check-'.$facility->id])}}
								@else
									{{Form::checkbox('facility_id[]', $facility->id, false, ['id' => 'check-'.$facility->id])}}
								@endif
								<label for="check-{{$facility->id}}"><span></span>{{$facility->title}}</label>
							</div>
							@endforeach
						</div>
						@else
							<div><p>No Facility available.</p></div>
						@endif
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			
			<div class="box box-orange">
				<div class="box-header with-border">
					<h3 class="box-title">Slider</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<!-- /.box-header -->
				<div class="box-body">

					<div class="gallery-wrapper">
					@foreach($accomodation->sliders()->order()->get() as $slider)
						<div class="single-gallery">
							<div class="form-group">
								<label class="control-label">Ordering<em class="asterisk">*</em></label>
								{{Form::number('gallery_ordering[]', $slider->ordering, ['class'=>'form-control', 'placeholder'=>'Enter Ordering', 'step'=>1, 'required', 'min'=>0])}}
							</div>
							
							{{Form::hidden('prev_image[]', $slider->image)}}
							<div class="form-group">
								<label class="control-label">Image<em class="asterisk">*</em></label>
								<div class="">
									<span class="btn btn-sm btn-karm btn-file ">
										<i class="fa fa-folder-open"></i> Upload Image
										<input type="file" name="gallery[]" accept="image/*" class="image-upload" onchange="readImageURL(this);">
									</span>
									<div class="show-img-bg mt-10" style="background-image: url({{asset('images/accomodations/'.$accomodation->id.'/'.'sliders/'.$slider->image)}})" alt="Image Preview"></div>
								</div>
								<div class="mt-10">
									<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-remove"><i class="fa fa-trash"></i> Delete</a>
								</div>
							</div>
						</div>
					@endforeach
					</div>
					<div class="btn-group">
						<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-add"><i class="fa fa-plus"></i> Add More</a>
					</div>

				</div>
				<!-- /.box-body -->
			</div>

			<div class="box collapsed-box">
				<div class="box-header with-border">
					<h3 class="box-title">SEO Settings</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<!-- /.box-header -->
				<div class="box-body">
					<div id="seo-block">
						<div class="form-group">
							<label class="control-label">Meta Title</label>
							{{Form::text('meta_title',null,['class'=>'form-control', 'placeholder'=>'Enter Meta Title'])}}
						</div>

						<div class="form-group">
							<label class="control-label">Meta Keyword</label>
							{{Form::textarea('meta_keyword',null,['class'=>'form-control', 'rows'=>'4', 'placeholder'=>'Enter Meta Keyword'])}}
						</div>

						<div class="form-group">
							<label class="control-label">Meta Description</label>
							{{Form::textarea('meta_desc',null,['class'=>'form-control',  'rows'=>'4', 'placeholder'=>'Enter Meta Description'])}}
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="col-md-3">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Status<em class="asterisk">*</em></h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					{{Form::select('status',['0' => 'Inactive', '1'=>'Active'],null,['class'=>'form-control', 'required'])}}
				</div><!-- /.box-body -->
			</div><!-- /.box -->

			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Ordering</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					{{Form::number('ordering', null, ['class'=>'form-control', 'placeholder'=>'Enter Ordering', 'step'=>1, 'min'=>0])}}
				</div><!-- /.box-body -->
			</div>

			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Header Slider<em class="asterisk">*</em></h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						{{Form::select('slider',['0' => 'Disable', '1'=>'Enable'],null,['class'=>'form-control'])}}
					</div>
					<div class="form-group">
						<label class="control-label">Select Slider</label>
						{{Form::select('slider_identifier',$sliders->toArray(),null,['class'=>'form-control'])}}
					</div>
				</div><!-- /.box-body -->
			</div>

			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Featured Image</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group featured-image">
						<span class="btn btn-sm btn-karm btn-file ">
							<i class="fa fa-folder-open"></i>Upload Image
							<input type="file" name="feat_img" class="form-control" onchange="readURL(this,'#preview2');" accept="image/*">
						</span>
						@if(!empty($accomodation->feat_img))
							<div id="preview2" class="show-img-bg" style="background-image: url({{asset('images/accomodations/'.$accomodation->id.'/'.$accomodation->feat_img)}})" alt="Image Preview"></div>
						@else
							<div id="preview2" class="show-img-bg display-none" alt="Image Preview"></div>
						@endif
					</div>
				</div>
			</div>

		   <div class="form-group">	
			   	{{Form::submit('Save',['class'=>'btn btn-karm'])}}
		   </div>
		</div>

   {{Form::close()}}
   </div>
     @include('backend.includes.tinymce')
   	


@endsection

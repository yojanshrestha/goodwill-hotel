<div class="pull-right mb-10 hidden-sm hidden-xs">
    <a href="{{url('admin/accomodations/create')}}" class="btn btn-info btn-xs">Create Accomodation</a>
    {{-- <a href="{{url('admin/accomodations')}}" class="btn btn-primary btn-xs">All Accomodations</a> --}}
</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Accomodations <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li><a href="{{url('admin/accomodations/create')}}">Create Accomodation</a></li>
            {{-- <li><a href="{{url('admin/accomodations')}}">All Accomodations</a></li> --}}
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
@extends('backend.layouts.app')

@section ('title', 'Blocks Management')

@section('page-header')
<h1>
	Edit Blocks
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li class="active">
		Blocks Management 
	</li>
</ol>
@endsection

@section('content')
	<div class="block-extra hide">
		<div class="single-block">
			<div class="col-md-9">
				<div class="box box-orange">
					<div class="box-header">
						<!-- <h3 class="box-title">Create Facility</h3> -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<div class="form-group">
							<label class="control-label">Title<em class="asterisk">*</em></label>
							{{Form::text('title[]',null,['class'=>'form-control', 'placeholder'=>'Enter Title', 'required'])}}
						</div>

						<div class="form-group">
							<label class="control-label">Slug</label>
							<div class="input-group">
							  <span class="input-group-addon">{{url('/').'/'}}</span>
							  {{Form::text('slug[]',null,['class'=>'form-control', 'placeholder'=>'Enter Slug'])}}
							</div>
							
						</div>

						<div class="form-group">
							<label class="control-label">Ordering</label>
							{{Form::number('ordering[]', null, ['class'=>'form-control', 'placeholder'=>'Enter Ordering', 'step'=>1, 'min'=>0])}}
						</div>

						<div class="form-group">
							<label class="control-label">Short Description</label>
								{!!Form::textarea('description[]',null,['class'=>'form-control', 'placeholder'=>'Enter Short Description', 'rows'=>8]) !!}
						</div>

						<div class="form-group">
								<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-remove"><i class="fa fa-trash"></i> Delete</a>
							</div>

					</div>
					<!-- /.box-body -->
				</div>
			</div>

			<div class="col-md-3">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">YouTube Embed URL</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div><!-- /.box-tools -->
					</div><!-- /.box-header -->
					<div class="box-body">
						{{Form::textarea('video[]', null, ['class'=>'form-control', 'placeholder'=>'Enter YouTube Embed URL', 'rows'=>5])}}
					</div><!-- /.box-body -->
				</div>

				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Image <small>(if there is not video url)</small></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div><!-- /.box-tools -->
					</div><!-- /.box-header -->
					<div class="box-body">
						<div class="form-group featured-image">
							<span class="btn btn-sm btn-karm btn-file ">
								<i class="fa fa-folder-open"></i>Upload Image
								<input type="file" name="feat_img[]" class="form-control" onchange="readImageURL(this);" accept="image/*">
							</span>
							{{Form::hidden('pre_image[]', null )}}
							<div class="show-img-bg display-none" alt="Image Preview"></div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
		{{ Form::open(['url'=>'admin/blocks', 'method'=>'PATCH', 'files'=> true, 'id' => 'blockForm']) }}


		<div class="row">
			<div class="block-wrapper">
				@foreach($blocks as $block)
					<div class="single-block">
						<div class="col-md-9">
							<div class="box box-orange">
								<div class="box-header">
									<!-- <h3 class="box-title">Create Facility</h3> -->
								</div>
								<!-- /.box-header -->
								<div class="box-body">

									<div class="form-group">
										<label class="control-label">Title<em class="asterisk">*</em></label>
										{{Form::text('title[]',$block->title,['class'=>'form-control', 'placeholder'=>'Enter Title', 'required'])}}
									</div>

									<div class="form-group">
										<label class="control-label">Slug</label>
										<div class="input-group">
										  <span class="input-group-addon">{{url('/').'/'}}</span>
										  {{Form::text('slug[]',$block->slug,['class'=>'form-control', 'placeholder'=>'Enter Slug'])}}
										</div>
										
									</div>

									<div class="form-group">
										<label class="control-label">Ordering</label>
										{{Form::number('ordering[]', $block->ordering, ['class'=>'form-control', 'placeholder'=>'Enter Ordering', 'step'=>1, 'min'=>0])}}
									</div>

									<div class="form-group">
										<label class="control-label">Short Description</label>
											{!!Form::textarea('description[]',$block->description,['class'=>'form-control', 'placeholder'=>'Enter Short Description', 'rows'=>8]) !!}
									</div>

									<div class="form-group">
											<a href="javascript:void(0)" class="btn btn-sm btn-danger btn-remove"><i class="fa fa-trash"></i> Delete</a>
										</div>

								</div>
								<!-- /.box-body -->
							</div>
						</div>

						<div class="col-md-3">
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">YouTube Embed URL</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div><!-- /.box-tools -->
								</div><!-- /.box-header -->
								<div class="box-body">
									{{Form::textarea('video[]', $block->video, ['class'=>'form-control', 'placeholder'=>'Enter YouTube Embed URL', 'rows'=>5])}}
								</div><!-- /.box-body -->
							</div>
							
							<div class="box box-default">
								<div class="box-header with-border">
									<h3 class="box-title">Image <small>(if there is not video url)</small></h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div><!-- /.box-tools -->
								</div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group featured-image">
										<span class="btn btn-sm btn-karm btn-file ">
											<i class="fa fa-folder-open"></i>Upload Image
											<input type="file" name="feat_img[]" class="form-control" onchange="readImageURL(this);" accept="image/*">
										</span>
										{{Form::hidden('pre_image[]', $block->image )}}
										@if(!empty($block->image))
											<div  class="show-img-bg" style="background-image: url({{asset($block->image)}})" alt="Image Preview"></div>
										@else
											<div class="show-img-bg display-none" alt="Image Preview"></div>
										@endif
									</div>
								</div>
							</div>

						</div>
					</div>
				@endforeach
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
					<a href="javascript:void(0)" class="btn btn-sm btn-primary btn-add"><i class="fa fa-plus"></i> Add More</a>
				   	{{Form::submit('Save',['class'=>'btn btn-warning btn-sm'])}}
			</div>
		</div>
<?php /*
		<div class="row">
			<div class="col-md-12">
				<div class="ads-section">
						{{-- {{dd($ads)}} --}}
					@foreach ($ads as $key => $ad)

						<div class="box box-orange ads-block">
							<div class="box-body">
								<div class="form-group">
									<label class="control-label">Ad<em class="asterisk">*</em></label>
									<br>
									<span class="btn btn-sm btn-karm btn-file">
										<i class="fa fa-folder-open"></i>Change
										<input type="file" name="ads[]" class="form-control ads-file">

										{{-- {{ Form::file('ads[]', ['id' => 'logo', 'class' => 'form-control ads-file']) }} --}}
									</span>
									<br><br>
									{{Form::hidden('pre_image[]', $ad->ads )}}
									<img class="img-center" width="479" height="341" id="logo_preview" src="{{asset($ad->ads)}}" alt="">
								</div>
								<div class="form-wrap form-group">
									<label for="ads_url">Ad Url</label>
									{{ Form::text('ads_url[]', $ad->ads_url, ['id' => '', 'class' => 'form-control', 'placeholder' => 'http://www.example.com']) }}
								</div>
								<div class="form-wrap form-group">
									<label for="ads_identifier">Identifier<em class="asterisk">*</em></label>
									{{ Form::text('ads_identifier[]', $ad->ads_identifier, ['id' => '', 'class' => 'form-control']) }}
								</div>
								<div class="form-wrap form-group">
									<a href="javascript:;" class="btn btn-sm btn-danger ad-delete">Delete</a>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<div class="form-wrap form-group">
					<a href="javascript:;" class="ad-Add btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add more</a>
				</div>
				<div class="form-group">	
					{{Form::submit('Update',['class'=>'btn btn-karm'])}}
				</div>
			</div>
		</div>
		*/ ?>
		{{Form::close()}}

@endsection
<div class="pull-right mb-10 hidden-sm hidden-xs">
    <a href="{{url('admin/facilities/create')}}" class="btn btn-info btn-xs">Create Facility</a>
    {{-- <a href="{{url('admin/facilities')}}" class="btn btn-primary btn-xs">All Facilities</a> --}}
</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Facilities <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li><a href="{{url('admin/facilities/create')}}">Create Facility</a></li>
            {{-- <li><a href="{{url('admin/facilities')}}">All Facilities</a></li> --}}
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                {{-- <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" /> --}}
                <img src="{{ asset('images/admin-user.png') }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- search form (Optional) -->
        {{-- {{ Form::open(['route' => 'admin.search.index', 'method' => 'get', 'class' => 'sidebar-form']) }}
        <div class="input-group">
            {{ Form::text('q', Request::get('q'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('strings.backend.general.search_placeholder')]) }}
            <span class="input-group-btn">
                <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span><!--input-group-btn-->
        </div><!--input-group-->
        {{ Form::close() }} --}}
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            @permission('view-pages-management')
                <li class="{{ active_class(Active::checkUriPattern('admin/pages*')) }} treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i><span>Page Manangement</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/pages*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/pages*'), 'display: block;') }}">
                        <li class="{{ active_class(Active::checkUriPattern('admin/pages/create')) }}">
                          <a href="{!! url('admin/pages/create') !!}"><i class="fa fa-plus"></i> Create</a>
                        </li>
                        <li class="{{ active_class(Active::checkUriPattern('admin/pages ')) }}">
                          <a href="{!! url('admin/pages') !!}"><i class="fa fa-file-text-o"></i> All Pages</a>
                        </li>
                    </ul>
                </li>
            @endauth

           
            @permission('view-menu-management')
                <li class="{{ active_class(Active::checkUriPattern('admin/menu')) }}">
                    <a href="{{ url('admin/menu') }}">
                        <i class="fa fa-list-ol"></i>
                        <span>Menu Management</span>
                    </a>
                </li>
            @endauth

            @permission('view-settings-management')
            <li class="{{ active_class(Active::checkUriPattern('admin/settings*')) }} treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i><span>Setting</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu {{active_class( Active::checkUriPattern('admin/settings*'),'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/settings*'), 'display: block;') }}">
                    <li class="{{ Active::checkUriPattern('admin/settings/general') }}">
                        <a href="{!! url('admin/settings/general') !!}"><i class="fa fa-cog"></i> General</a>
                    </li>
                </ul>
            </li>
            @endauth


            @permission('view-slider-management')
            <li class="{{ active_class(Active::checkUriPattern('admin/sliders*')) }} treeview">
                <a href="#">
                    <i class="fa fa-image"></i><span>Slider Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu {{active_class( Active::checkUriPattern('admin/sliders*'),'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/sliders*'), 'display: block;') }}">                    
                    <li class="{{ active_class(Active::checkUriPattern('admin/sliders/create')) }} treeview">
                        <a href="{!! url('admin/sliders/create') !!}"><i class="fa fa-plus"></i> Create</a>
                    </li>
                    <li class="{{ Active::checkUriPattern('admin/sliders/view') }}">
                        <a href="{!! url('admin/sliders') !!}"><i class="fa fa-file-text-o"></i> All Sliders</a>
                    </li>
                </ul>
            </li>
            @endauth

            @permission('view-blocks-management')
                <li class="{{ active_class(Active::checkUriPattern('admin/blocks')) }}">
                    <a href="{{ url('admin/blocks') }}">
                        <i class="fa fa-clone"></i>
                        <span>Blocks Management</span>
                    </a>
                </li>
            @endauth

            @permission('view-facilities-management')
            <li class="{{ active_class(Active::checkUriPattern('admin/facilities*')) }} treeview">
                <a href="#">
                    <i class="fa fa-tasks"></i><span>Facilities Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu {{active_class( Active::checkUriPattern('admin/facilities*'),'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/facilities*'), 'display: block;') }}">                    
                    <li class="{{ active_class(Active::checkUriPattern('admin/facilities/create')) }} treeview">
                        <a href="{!! url('admin/facilities/create') !!}"><i class="fa fa-plus"></i> Create</a>
                    </li>
                    <li class="{{ Active::checkUriPattern('admin/facilities/view') }}">
                        <a href="{!! url('admin/facilities') !!}"><i class="fa fa-file-text-o"></i> All Facilities</a>
                    </li>
                </ul>
            </li>
            @endauth

            @permission('view-accomodations-management')
            <li class="{{ active_class(Active::checkUriPattern('admin/accomodations*')) }} treeview">
                <a href="#">
                    <i class="fa fa-hotel"></i><span>Accomodations Management</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                
                <ul class="treeview-menu {{active_class( Active::checkUriPattern('admin/accomodations*'),'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/accomodations*'), 'display: block;') }}">                    
                    <li class="{{ active_class(Active::checkUriPattern('admin/accomodations/create')) }} treeview">
                        <a href="{!! url('admin/accomodations/create') !!}"><i class="fa fa-plus"></i> Create</a>
                    </li>
                    <li class="{{ Active::checkUriPattern('admin/accomodations/view') }}">
                        <a href="{!! url('admin/accomodations') !!}"><i class="fa fa-file-text-o"></i> All Accomodations</a>
                    </li>
                </ul>
            </li>
            @endauth

            @permission('view-testimonials-management')
                <li class="{{ active_class(Active::checkUriPattern('admin/testimonials*')) }} treeview">
                    <a href="#">
                        <i class="fa fa-quote-left"></i><span>Testimonial Manangement</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/testimonials*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/testimonials*'), 'display: block;') }}">
                        <li class="{{ active_class(Active::checkUriPattern('admin/testimonials/create')) }}">
                          <a href="{!! url('admin/testimonials/create') !!}"><i class="fa fa-plus"></i> Create</a>
                        </li>
                        <li class="{{ active_class(Active::checkUriPattern('admin/testimonials')) }}">
                          <a href="{!! url('admin/testimonials') !!}"><i class="fa fa-file-text-o"></i> All Testimonials</a>
                        </li>
                    </ul>
                </li>
            @endauth

            @role(1)
            <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth

        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
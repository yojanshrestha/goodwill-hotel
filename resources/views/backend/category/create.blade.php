@extends('backend.layouts.app')

@section ('title', 'Create Menu')

@section('page-header')
<h1>
	Menu Management
	<!-- <small>Lists of Pages</small> -->
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Menu</a></li>
	<li class="active">
		Create Menu
	</li>
</ol>
@endsection

@section('content')

@if($errors->has('banner_title[0]'))
	erro
@endif

{{ Form::open(['url'=>'admin/menu/new','id'=>'categoryForm','role'=>'form', 'files'=>'true'])}}
<div class="row">
	<div class="col-md-9"> 

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Create Menu</h3>
				<a href="{{ url('admin/menu')}}" class="btn btn-warning btn-sm pull-right">Go Back</a>
			</div><!-- /.box-header -->
			<div class="box-body">
					<div class="form-group">
						<label for="title" class="control-label">Title<em class="asterisk">*</em></label>
							{!! Form::text('title',null,array('class'=>'form-control', 'placeholder'=>'Enter Title'))!!}
					</div>
		            <?php /*  ?>
		            <div class="form-group">
		                <label for="label" class="col-lg-2 control-label">Label</label>
		                <div class="col-lg-10">
		                  {{ Form::text('label',null,array('class'=>'form-control', 'required'))}}
		                </div>
		            </div>
		            <?php */ ?>
		            <div class="form-group">
		            	<label for="url" class="control-label">URL</label>
		            	<div class="input-group">
						  <span class="input-group-addon">{{url('/'). '/'}}</span>
		            		{{ Form::text('url',null,array('class'=>'form-control', 'placeholder'=>'Enter URL'))}}
						</div>
		            </div>

			</div><!-- /.box-body -->
		</div><!-- /.box -->
		
	</div>

	<div class="col-md-3">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Status<em class="asterisk">*</em></h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					{{Form::select('status',['0' => 'Inactive', '1'=>'Active'],null,['class'=>'form-control'])}}
				</div><!-- /.box-body -->
			</div><!-- /.box -->

		   <div class="form-group">	
			   	{{Form::submit('Save',['class'=>'btn btn-karm btn-sm'])}}
		   </div>
		</div>
</div>
{{ Form::close()}}

@stop
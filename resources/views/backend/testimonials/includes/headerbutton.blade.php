<div class="pull-right mb-10 hidden-sm hidden-xs">
    <a href="{{url('admin/testimonials/create')}}" class="btn btn-info btn-xs">Create Testimonial</a>
    {{-- <a href="{{url('admin/testimonials')}}" class="btn btn-primary btn-xs">All Testimonial</a> --}}
</div><!--pull right-->

<div class="pull-right mb-10 hidden-lg hidden-md">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            Testimonial <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li><a href="{{url('admin/testimonials/create')}}">Create Testimonial</a></li>
            {{-- <li><a href="{{url('admin/testimonials')}}">All Testimonial</a></li> --}}
        </ul>
    </div><!--btn group-->
</div><!--pull right-->

<div class="clearfix"></div>
@extends('backend.layouts.app')

@section ('title', 'Edit Testimonial')

@section('page-header')
<h1>
	Edit Testimonial
</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Testimonial</a></li>
		<li class="active">
			Edit Testimonial
		</li>
	</ol>
@endsection

@section('content')
<!-- Main content -->
	<div class="row">
		{{Form::model($testimonial,['url'=>'admin/testimonials/'.$testimonial->id, 'method' =>'patch'])}}
		<div class="col-md-9">
			<div class="box box-orange">
				<div class="box-header">
					<!-- <h3 class="box-title">Create Page</h3> -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="form-group">
						<label class="control-label">Name<em class="asterisk">*</em></label>
						{{Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Enter Name'])}}
					</div>

					<div class="form-group">
						<label class="control-label">Address<em class="asterisk">*</em></label>
						{{Form::text('address',null,['class'=>'form-control', 'placeholder'=>'Enter Address'])}}
					</div>

					<div class="form-group">
						<label class="control-label">Review<em class="asterisk">*</em></label>
							{!!Form::textarea('review',null,['class'=>'form-control', 'placeholder'=>'Enter Review', 'rows'=>5]) !!}
					</div>

					</div>
				<!-- /.box-body -->
			</div>
			
		</div>

		<div class="col-md-3">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Status<em class="asterisk">*</em></h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					{{Form::select('status',['0' => 'Inactive', '1'=>'Active'],null,['class'=>'form-control'])}}
				</div><!-- /.box-body -->
			</div><!-- /.box -->

			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Ordering<em class="asterisk">*</em></h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div><!-- /.box-tools -->
				</div><!-- /.box-header -->
				<div class="box-body">
					{{Form::number('ordering',null,['class'=>'form-control ordering','placeholder'=>'Enter Ordering', 'step'=>1, 'min'=>0])}}
				</div><!-- /.box-body -->
			</div>

		   <div class="form-group">	
			   	{{Form::submit('Save',['class'=>'btn btn-karm'])}}
		   </div>
		</div>

   	{{Form::close()}}
   	</div>
    @include('backend.includes.tinymce')

@endsection

<!DOCTYPE html>
<html>
    <head>
      <meta charset="UTF-8">
        <title>@yield('title')@if(!empty($settings->tagline)) | {{$settings->tagline}}@endif</title>
        {{-- SEO --}}
        <meta name="meta_title" content="@yield('meta_title')">
        <meta name="meta_description" content="@yield('meta_description')">
        <meta name="meta_keyword" content="@yield('meta_keyword')">
        {{-- Social Sharing Facebook--}}
        {{-- <meta property="og:title" content="@yield('og_title')" />
        <meta property="og:url" content="@yield('og_url')" />
        <meta property="og:image" content="@yield('og_image')" />
        <meta property="og:description" content="@yield('og_description')" /> --}}
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable = no">
        <link rel="shortcut icon" href="@if(!empty($settings->favicon)){{asset('/images/logo/favicon/'.$settings->favicon)}}@endif">
        <link rel="apple-touch-icon" href="@if(!empty($settings->favicon)){{asset('/images/logo/favicon/'.$settings->favicon)}}@endif">
        <link rel="stylesheet" href="{{ asset('/css/frontend/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/frontend/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/frontend/fontawesome.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/frontend/lightcase.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/frontend/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/frontend/responsive.css') }}">
        <script type="text/javascript">
            var base_url = "{{ url('/') }}";
        </script>
    </head>

    <body class="{{ !empty($class) ? $class : '' }}">
        
        @include('frontend/includes/slider')
        
        @include('frontend/includes/header')

        @yield('content')

        {{-- @include('frontend/includes/socialinfo') --}}

        @include('frontend/includes/footer')

        {{-- <div class="loader" style="display: none;">
            <img src="{{asset('/images/processing.gif')}}" width="50" height="50">
        </div> --}}
      
        {{-- Javascripts --}}

        <script type="text/javascript" src="{{ asset('/js/frontend/jquery-3.3.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/bootstrap.min.js') }}"></script>   
        <script type="text/javascript" src="{{ asset('/js/frontend/owl.carousel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/lightcase.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/jquery.waypoints.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/sticky.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/backend/jquery.validate.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/js/frontend/scripts.js') }}"></script>

        <!-- <script type="text/javascript" src="{{ asset('/js/frontend/custom.js') }}"></script> -->

    </body>
</html>
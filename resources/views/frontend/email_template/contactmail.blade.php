<div style="margin: 0 auto; padding: 50px 0; width: 100%;">
<center>
<table style="width: 600px; margin: 0px auto; background: #fff; padding: 0px; border: 1px solid #ececec;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="logo">
<td style="padding: 0 20px 10px; border-bottom: 1px dashed #500847; margin: 0;"><img class="w320" src="{{asset('/images/logo/'.$setting->logo)}}" alt="company logo" height="100" /></td>
</tr>
<tr class="main-content" style="padding: 0; margin: 0;">
<td style="font-size: 14px; padding: 20px 20px 0px; font-weight: 600; font-family: Arial; margin-top: 10px;">
<p style="padding: 0 0 5px 0; margin: 0;">Hello Admin,<br /><br /></p>
</td>
</tr>
<tr class="mobile-spacing" style="font-size: 14px; padding: 10px 20px; margin: 0; font-family: Arial;">
<td style="padding: 0px 20px 20px 20px;">
<p style="padding: 0 0 5px 0; margin: 0;">The following user have contact you. Please confirm the information.</p>
</td>
</tr><tr class="mobile-spacing" style="font-size: 14px; padding: 10px 20px; margin: 0; font-family: Arial;">
<td style="padding: 0px 20px 20px 20px;">
<p style="padding: 0 0 5px 0; margin: 0;">Name: {{ $user }}</p>
</td>
</tr><tr class="mobile-spacing" style="font-size: 14px; padding: 10px 20px; margin: 0; font-family: Arial;">
<td style="padding: 0px 20px 20px 20px;">
<p style="padding: 0 0 5px 0; margin: 0;">Email: {{ $email }}</p>
</td>
</tr><tr class="mobile-spacing" style="font-size: 14px; padding: 10px 20px; margin: 0; font-family: Arial;">
<td style="padding: 0px 20px 20px 20px;">
<p style="padding: 0 0 5px 0; margin: 0;">Subject: {{ $subject }}</p>
</td>
</tr><tr class="mobile-spacing" style="font-size: 14px; padding: 10px 20px; margin: 0; font-family: Arial;">
<td style="padding: 0px 20px 20px 20px;">
<p style="padding: 0 0 5px 0; margin: 0;">Message: {!! $content !!}</p>
</td>
</tr>
</tbody>
</table>
</center>
</div>
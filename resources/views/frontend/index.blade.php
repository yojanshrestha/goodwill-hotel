@extends('frontend/layouts/app')

@section('title'){{$settings->title}}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content">
    <div class="container">
        @if(count($blocks) > 0)
        <div class="section feature-overview">
            <div class="row">
                @foreach($blocks as $block)
                <div class="col-lg-4">
                    <div class="thumbnail">
                        <div class="thumb-hover">
                            @if(!empty($block->video))
                            <?php
                                $videoId = getYouTubeVideoId($block->video);
                            ?>
                            <div class="image-holder bg-cover" style="background-image: url({{ (!empty($videoId))? 'http://img.youtube.com/vi/'.$videoId.'/sddefault.jpg' : '' }});">
                            </div>
                            <a href="{{$block->video}}" data-rel="lightcase" class="play"><i class="fas fa-play-circle"></i></a>
                            @else
                            <div class="image-holder bg-cover" style="background-image: url({{ ((!empty($block->image))? asset($block->image) : '') }});">
                            </div>
                            @endif
                        </div>
                        <div class="thumb-body">
                            <h5 class="title">{{$block->title}}</h5>
                            <p>{!! $block->description !!}</p>
                            @if(!empty($block->slug))
                            <a href="{{url($block->slug)}}" class="btn btn-dark">view all</a>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif

        @if(count($testimonials) > 0)
            <div class="section testimonials mt-100">
                <div class="section-title">
                    <h4>client review</h4>
                </div>
                <div class="owl-carousel owl-theme">
                    @foreach($testimonials as $testimonial)
                        <div class="item">
                            <div class="auther-text">
                                <p>{{ $testimonial->review }}</p>
                            </div>
                            <div class="auther-info">
                                <h5>{{ $testimonial->name }}</h5>
                                <span>{{ $testimonial->address }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        <div class="section"></div>
    </div> 

@endsection
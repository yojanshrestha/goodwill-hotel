@extends('frontend/layouts/app')

@section('title'){{$page->title}}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content onload-top">
	<div class="container">
		<div class="section rooms dynamic-content">
			<div class="row">
				<div class="col-lg-6">
					<h5 class="title">drop us a line</h5>
						@if(!empty($page->top_content))
							{!! $page->top_content !!}
						@endif
					
					<form action="javascript:;" class="contact-form" id="contactform" name="contactform">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>name</label>
									<input type="text" class="form-control required" name="username" value="">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>email</label>
									<input type="text" class="form-control required" name="emailadd" value="">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label>subject</label>
									<input type="text" class="form-control required" name="subject" value="">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label>message</label>
									<textarea class="form-control required" rows="5" name="message" value=""></textarea>
								</div>
							</div>
							<div class="col-lg-12">
								<input type="submit" value="submit" class="btn btn-red">
								<img src="{{ asset('images/loading.gif') }}" width="20" height="20" style="display: none;" class="loaderimg">
								<div class="success"></div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-lg-6">
					@if(!empty($settings->iframe))
						<div class="map">
							{!! $settings->iframe !!}
							{{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.782450657926!2d83.95762181449886!3d28.21392148258479!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399594e212bf325f%3A0xc6f2ad2541fcde42!2sOYO+11475+Hotel+Pokhara+Goodwill!5e0!3m2!1sen!2snp!4v1542689919259" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
						
						</div>
					@endif
					<div class="contact-info">
						<ul>
							@if(!empty($settings->address))
								<li>
									<strong>address</strong>
									<span>{{ $settings->address }}</span>
								</li>
							@endif
							@if(!empty($settings->contactno))
							<li>
								<strong>phone</strong>
								<span>{{ $settings->contactno }}</span>
							</li>
							@endif
							@if(!empty($settings->contact_email))
							<li>
								<strong>email</strong>
								<span class="text-lowercase"><a href="mailto:{{$settings->contact_email}}">{{ $settings->contact_email }}</a></span>
							</li>
							@endif

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> 

@endsection


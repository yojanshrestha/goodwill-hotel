@extends('frontend/layouts/app')

@section('title'){{$page->title}}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')
		
<div class="main-content onload-top">
	<div class="container dynamic-content about-content">
		<h2 class="page-title text-center">{{ $page->title }}</h2>
		<div class="intro">
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					@if(!empty($page->top_content))
						<div class="description text-center">
							{!! $page->top_content !!}
						</div>
					@endif
				</div>				
			</div>
		</div>

		@if(count($facilities) > 0)
			<div class="section services">
				@foreach($facilities as $key => $facility )
					@if($key % 2 == 0)
						<div class="row block-row">
							@if(!empty($facility->feat_img))
								<div class="col-lg-6">
									<div class="image-holder bg-cover" style="background-image: url({{ asset('/images/facilities/'.$facility->id.'/'.$facility->feat_img) }});">
									</div>
								</div>
							@endif
							<div class="{{ !empty($facility->feat_img) ? 'col-lg-6' : 'col-lg-12' }}">
								<div class="description">
									<h5 class="title">{{ $facility->title }}</h5>
									@if(!empty($facility->content))
										{!! $facility->content !!}
									@endif
								</div>
							</div>
						</div>
					@else
						<div class="row block-row flex-row-reverse">
							@if(!empty($facility->feat_img))
								<div class="col-lg-6">
									<div class="image-holder bg-cover" style="background-image: url({{ asset('/images/facilities/'.$facility->id.'/'.$facility->feat_img) }});">
									</div>
								</div>
							@endif
							<div class="{{ !empty($facility->feat_img) ? 'col-lg-6' : 'col-lg-12' }}">
								<div class="description">
									<h5 class="title">{{ $facility->title }}</h5>
									@if(!empty($facility->content))
										{!! $facility->content !!}
									@endif
								</div>
							</div>
						</div>
					@endif
				@endforeach
			</div>
		@endif
	</div>
@endsection
	
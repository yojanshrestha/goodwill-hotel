@extends('frontend/layouts/app')

@section('title'){{ $page->title }}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content onload-top">
	<div class="container dynamic-content facilities-content">
		<div class="intro">
			<div class="row">
				@if(count($feat_facility) > 0)
					<div class="{{ count($feat_facility->sliders) > 0 || !empty($feat_facility->feat_img) ? 'col-lg-6' : 'col-lg-12' }}">
						<h2 class="title">{{ $feat_facility->title }}</h2>
						<div class="description">
							<div class="description-content">
								{!! $feat_facility->content !!}
							</div>
							@if(count($feat_facility->features) > 0)
								<ul class="check-list">
									@foreach($feat_facility->features as $feat_facility_feature)
										<li>{{ $feat_facility_feature->title }}</li>
									@endforeach
								</ul>
							@endif
						</div>
					</div>
					@if(count($feat_facility->sliders) > 0)
						<div class="col-lg-6">
							<div class="feature-slider">
								<div class="owl-theme owl-carousel">
									@foreach($feat_facility->sliders as $feat_facility_slider)
										<div class="item bg-cover" style="background-image: url({{ asset('/images/facilities/'.$feat_facility_slider->facility_id.'/sliders/'.$feat_facility_slider->image) }});">
										</div>
									@endforeach
								</div>
							</div>
						</div>
					@endif
				@endif
			</div>
		</div>
		@if(count($other_facilities) > 0)
			<div class="section other-facilities">
				<h5 class="title">other facilities</h5>
				<div class="owl-carousel owl-theme">
					@foreach($other_facilities as $other_facility)
						<div class="item">
							<a href="{{ url('/facility/'.$other_facility->slug) }}" class="thumb-image sm-thumb " >
								<div class="image-holder bg-cover" style="background-image: url(@if(!empty($other_facility->feat_img)) {{ asset('/images/facilities/'.$other_facility->id.'/'.$other_facility->feat_img) }} @endif);">
								</div>
								<div class="thumb-caption">
									<h6 class="thumb-title">{{ $other_facility->title }}</h6>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
		@endif
	</div>

@endsection
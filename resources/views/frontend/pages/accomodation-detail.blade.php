@extends('frontend/layouts/app')

@section('title'){{ $accomodation->title }}@endsection
@section('meta_title')
@if(!empty($accomodation->meta_title)){{ $accomodation->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($accomodation->meta_desc)){{ $accomodation->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($accomodation->meta_keyword)){{ $accomodation->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content onload-top">
	<div class="container dynamic-content facilities-content">
		<div class="intro">
			<div class="row">
				<div class="{{ count($accomodation->sliders) > 0 ? 'col-lg-6' : 'col-lg-12' }}">
					<h2 class="title">{{ $accomodation->title }}</h2>
					<div class="description">
						<div class="description-content">
							{!! $accomodation->content !!}
						</div>
						@if(count($accomodation->facilities) > 0)
							<ul class="check-list">
								@foreach($accomodation->facilities as $accomodation_facility)
									<li>{{ $accomodation_facility->title }}</li>
								@endforeach
							</ul>
						@endif
						{{-- <a href="#" class="btn btn-lg btn-red">book this room</a> --}}
					</div>
				</div>

				@if(count($accomodation->sliders) > 0)
					<div class="col-lg-6">
						<div class="feature-slider">
							<div class="owl-theme owl-carousel">
								@if(count($accomodation->sliders) > 0)
									@foreach($accomodation->sliders as $accomodation_slider)
										<div class="item bg-cover" style="background-image: url({{ asset('/images/accomodations/'.$accomodation_slider->accomodation_id.'/sliders/'.$accomodation_slider->image) }});"></div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>

@endsection


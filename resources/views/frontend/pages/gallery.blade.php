@extends('frontend/layouts/app')

@section('title'){{ $page->title }}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content">

@endsection
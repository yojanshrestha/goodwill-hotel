@extends('frontend/layouts/app')

@section('title'){{ $page->title }}@endsection
@section('meta_title')
@if(!empty($page->meta_title)){{ $page->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($page->meta_desc)){{ $page->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($page->meta_keyword)){{ $page->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content onload-top">
	<div class="container dynamic-content">
		<div class="rooms">
			<div class="row">
				@if(count($accomodations) > 0)
					@foreach($accomodations as $accomodation)
						<div class="col-lg-4">
							<a href="{{ url('/accomodation/'.$accomodation->slug) }}" class="thumb-image " >
								<div class="image-holder bg-cover" style="background-image: url(@if(!empty($accomodation->feat_img)) {{ asset('images/accomodations/'.$accomodation->id.'/'.$accomodation->feat_img) }} @endif);">
								</div>
								<div class="thumb-caption">
									<h5 class='thumb-title'>{{ $accomodation->title }}</h5 class='thumb-title'>
								</div>
							</a>
						</div>
					@endforeach
				@endif
			</div>
		</div>
	</div> 
@endsection
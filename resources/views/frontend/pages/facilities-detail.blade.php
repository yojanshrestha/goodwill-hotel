@extends('frontend/layouts/app')

@section('title'){{ $facility->title }}@endsection
@section('meta_title')
@if(!empty($facility->meta_title)){{ $facility->meta_title }} @else{{ $settings->meta_title }}@endif
@endsection
@section('meta_description')
@if(!empty($facility->meta_desc)){{ $facility->meta_desc }}@else{{ $settings->meta_desc }}@endif
@endsection
@section('meta_keyword')
@if(!empty($facility->meta_keyword)){{ $facility->meta_keyword }}@else{{ $settings->meta_keyword }}@endif
@endsection

@section('content')

<div class="main-content onload-top">
	<div class="container dynamic-content facilities-content">
		<div class="intro">
			<div class="row">
				<div class="{{ count($facility->sliders) > 0 ? 'col-lg-6' : 'col-lg-12' }}">
					<h2 class="title">{{ $facility->title }}</h2>
					<div class="description">
						<div class="description-content">
							{!! $facility->content !!}
						</div>
						@if(count($facility->features) > 0)
							<ul class="check-list">
								@foreach($facility->features as $facility_feature)
									<li>{{ $facility_feature->title }}</li>
								@endforeach
							</ul>
						@endif
					</div>
				</div>
				@if(count($facility->sliders) > 0)
					<div class="col-lg-6">
						<div class="feature-slider">
							<div class="owl-theme owl-carousel">
								@if(count($facility->sliders) > 0)
									@foreach($facility->sliders as $facility_slider)
										<div class="item bg-cover" style="background-image: url({{ asset('/images/facilities/'.$facility_slider->facility_id.'/sliders/'.$facility_slider->image) }});">
										</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
		@if(count($other_facilities) > 0)
			<div class="section other-facilities">
				<h5 class="title">other facilities</h5>
				<div class="owl-carousel owl-theme">
					@foreach($other_facilities as $other_facility)
						<div class="item">
							<a href="{{ url('/facility/'.$other_facility->slug) }}" class="thumb-image sm-thumb " >
								<div class="image-holder bg-cover" style="background-image: url(@if(!empty($other_facility->feat_img)) {{ asset('/images/facilities/'.$other_facility->id.'/'.$other_facility->feat_img) }} @endif);">
								</div>
								<div class="thumb-caption">
									<h6 class="thumb-title">{{ $other_facility->title }}</h6>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
		@endif
	</div>

@endsection
<div class="top-content align-items-center">
	<a href="{{ url('/') }}" class="logo">
		<img src="@if(!empty($settings->logo)) {{ asset('/images/logo/'.$settings->logo) }} @endif" alt="Logo">
	</a>
	<div class="right-content">
		<ul class="inline-list">
			@if(count($settings) > 0 && !empty($settings->contactno))
				<li class="phone">
					<p>
						<i class="fas fa-phone"></i> {{ $settings->contactno }}
					</p>
				</li>
			@endif
			{{-- <li><a href="#" class="btn btn-red btn-lg">book now</a></li> --}}
		</ul>
	</div>
</div>

<div class="navigation">
	<div class="container-fluid">
		<div class="row align-items-center">
			@if(count($settings) > 0)
				<div class="col-lg-4">
					<ul class="social-list inline-list">
						@if(!empty($settings->facebook))
							<li><a href="{{ $settings->facebook }}" target="@if($settings->facebook != '#'){{'_blank'}}@endif"><i class="fab fa-facebook-f"></i></a></li>
						@endif
						@if(!empty($settings->twitter))
							<li><a href="{{ $settings->twitter }} " target="@if($settings->twitter != '#'){{'_blank'}}@endif"><i class="fab fa-twitter"></i></a></li>
						@endif
						@if(!empty($settings->instagram))
							<li><a href="{{ $settings->instagram }}" target="@if($settings->instagram != '#'){{'_blank'}}@endif"><i class="fab fa-instagram"></i></a></li>
						@endif
						@if(!empty($settings->youtube))
							<li><a href="{{ $settings->youtube }}" target="@if($settings->youtube != '#'){{'_blank'}}@endif"><i class="fab fa-youtube"></i></a></li>
						@endif
					</ul>
				</div>
			@endif
			@if(count($categories) > 0)
				<div class="col-lg-8">
					<ul class="main-nav inline-list">
						@foreach($categories as $menu)
							<li class="{{ Request::is($menu->url) ? 'active' : '' }}">
								<a href="{{ url($menu->url) }}">{{ $menu->title }}</a>
								@if(count($menu->activeChildren) > 0)
									<ul class='mega-menu'>
										@foreach($menu->activeChildren as $submenu)
											<li class="{{ Request::is($submenu->url) ? 'active' : '' }}">
												<a href="{{ url($submenu->url) }}">{{ $submenu->title }}</a>
											</li>
										@endforeach
									</ul>
								@endif
							</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	</div>
</div>

{{-- Main sliders --}}
@if(!empty($slides) && count($slides) > 0 )
	<div class="slider">
		<div class="owl-carousel owl-theme">
			@foreach($slides as $slide)
				<div class="item bg-cover overlay-dark" style="background-image: url({{ asset($slide->Slider_image) }});">
				</div>
			@endforeach
		</div>
	</div>
@endif
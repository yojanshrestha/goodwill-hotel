	<footer>
		@if(count($footermenus) > 0)
			<div class="menu-footer">
				<div class="container">
					<ul class="inline-list mb-0">
						@foreach($footermenus as $footermenu)
							<li><a href="{{ url($footermenu->slug) }}">{{ $footermenu->title }}</a></li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		<div class="main-footer section">
			<div class="container">
				<div class="footer-logo text-center">
					<h2>{{ $settings->title }}</h2>
					<div class="stars">
						<ul class="inline-list">
							<li><i class="fas fa-star"></i></li>
							<li><i class="fas fa-star"></i></li>
							<li><i class="fas fa-star"></i></li>
							<li><i class="fas fa-star"></i></li>
						</ul>
					</div>
				</div>
				<div class="footer-social">
					@if(count($settings) > 0)
						<ul class="inline-list mb-0" >
							@if(!empty($settings->facebook))
								<li>
									<a href="{{ $settings->facebook }}" target="@if($settings->facebook != '#'){{'_blank'}}@endif" class="fb"><i class="fab fa-facebook-f"></i></a>
								</li>
							@endif
							@if(!empty($settings->instagram))
								<li>
									<a href="{{ $settings->instagram }}" target="@if($settings->instagram != '#'){{'_blank'}}@endif" class="insta"><i class="fab fa-instagram"></i></a>
								</li>
							@endif
						</ul>
					@endif
					@if(!empty($settings->footer_content))
						{!! $settings->footer_content !!}
					@endif
				</div>
				<div class="footer_copyright text-center">
					<p>COPYRIGHT © HOTEL POKHARA GOOWILL. ALL RIGHT RESERVED | Powered by <a href="#">Excel It solution</a></p>
				</div>
			</div>	
		</div>
		
	</footer>
</div>
-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2018 at 11:57 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goodwill`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `parent_id`, `title`, `label`, `url`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Home', '', '/', 0, 1, '2018-12-20 09:45:38', '2018-12-26 16:27:51'),
(2, 0, 'Accomodation', '', 'accomodation', 2, 1, '2018-12-20 09:46:04', '2018-12-25 07:01:26'),
(3, 0, 'About', '', 'about', 5, 1, '2018-12-20 09:46:15', '2018-12-25 07:01:42'),
(4, 0, 'Contact', '', 'contact', 7, 1, '2018-12-20 09:46:50', '2018-12-25 07:01:43'),
(5, 0, 'Facilities', '', 'facilities', 4, 1, '2018-12-20 09:47:35', '2018-12-25 07:01:42'),
(6, 2, 'Deluxe Room', '', 'accomodation/deluxe-room', 1, 1, '2018-12-20 09:48:18', '2018-12-28 09:56:03'),
(7, 2, 'Super Deluxe Room', '', 'accomodation/super-deluxe-room', 0, 1, '2018-12-20 09:49:24', '2018-12-28 09:55:57'),
(8, 2, 'Normal Room', '', 'accomodation/normal-room', 2, 1, '2018-12-20 09:50:07', '2018-12-28 09:56:16'),
(9, 5, 'Restaurant', '', 'facility/restaurant', 0, 1, '2018-12-20 10:51:35', '2018-12-28 09:53:50'),
(10, 5, 'Wifi', '', 'facility/wifi', 1, 1, '2018-12-20 10:51:49', '2018-12-28 09:54:25'),
(11, 5, 'Swimming Pool', '', 'facility/swimming-pool', 2, 1, '2018-12-20 10:52:05', '2018-12-28 09:54:32'),
(12, 5, 'Conference Room', '', 'facility/conference-room', 3, 1, '2018-12-20 10:52:18', '2018-12-28 09:54:38'),
(13, 5, '24/7 Kitchen', '', '247-kitchen', 4, 1, '2018-12-20 10:52:34', '2018-12-20 10:53:01'),
(14, 0, 'Gallery', '', 'gallery', 6, 1, '2018-12-20 10:53:13', '2018-12-25 07:01:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

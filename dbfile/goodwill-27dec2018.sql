-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2018 at 07:47 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goodwill`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodations`
--

CREATE TABLE `accomodations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text,
  `feat_img` varchar(255) DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `slider` tinyint(1) NOT NULL DEFAULT '0',
  `slider_identifier` varchar(50) DEFAULT NULL,
  `meta_title` varchar(500) DEFAULT NULL,
  `meta_desc` text,
  `meta_keyword` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodations`
--

INSERT INTO `accomodations` (`id`, `title`, `slug`, `content`, `feat_img`, `ordering`, `status`, `slider`, `slider_identifier`, `meta_title`, `meta_desc`, `meta_keyword`, `created_at`, `updated_at`) VALUES
(1, 'deluxe room', 'deluxe-room', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet porro quae quasi temporibus qui facere enim earum voluptas ratione! Aspernatur nobis odio maiores velit et. Itaque, excepturi! Odit, magni reprehenderit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet porro quae quasi temporibus qui facere enim earum voluptas ratione! Aspernatur nobis odio maiores velit et. Itaque, excepturi! Odit, magni reprehenderit.</p>', 'eBcWx-IMG_1270.jpg', 1, 1, 1, '8nlX8YhSub', 'Deluxe Room', 'best rooms available, hotel goodwill pokhara', 'Best deluxe room', '2018-12-25 00:58:30', '2018-12-27 01:00:33'),
(2, 'Normal Room', 'normal-room', NULL, 'iIxEF-IMG_1194.jpg', 3, 1, 0, NULL, NULL, NULL, NULL, '2018-12-25 01:03:52', '2018-12-25 01:03:52');

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_facility`
--

CREATE TABLE `accomodation_facility` (
  `accomodation_id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodation_facility`
--

INSERT INTO `accomodation_facility` (`accomodation_id`, `facility_id`) VALUES
(1, 9),
(2, 7),
(2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `accomodation_sliders`
--

CREATE TABLE `accomodation_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `accomodation_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accomodation_sliders`
--

INSERT INTO `accomodation_sliders` (`id`, `accomodation_id`, `image`, `ordering`, `created_at`, `updated_at`) VALUES
(1, 1, 'cTZG5-IMG_1270.jpg', 1, '2018-12-25 00:58:31', '2018-12-25 00:58:31'),
(2, 1, '9QtAh-IMG_1394.jpg', 2, '2018-12-25 00:58:32', '2018-12-25 00:58:32'),
(3, 2, 'bx212-IMG_1270.jpg', 1, '2018-12-25 01:03:52', '2018-12-25 01:03:52'),
(4, 2, 'bof63-IMG_1279.jpg', 2, '2018-12-25 01:03:52', '2018-12-25 01:03:52');

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `parent_id`, `title`, `label`, `url`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Home', '', '/', 0, 1, '2018-12-20 09:45:38', '2018-12-26 16:27:51'),
(2, 0, 'Accomodation', '', 'accomodation', 2, 1, '2018-12-20 09:46:04', '2018-12-25 07:01:26'),
(3, 0, 'About', '', 'about', 5, 1, '2018-12-20 09:46:15', '2018-12-25 07:01:42'),
(4, 0, 'Contact', '', 'contact', 7, 1, '2018-12-20 09:46:50', '2018-12-25 07:01:43'),
(5, 0, 'Facilities', '', 'facilities', 4, 1, '2018-12-20 09:47:35', '2018-12-25 07:01:42'),
(6, 2, 'Deluxe Room', '', 'deluxe-room', 1, 1, '2018-12-20 09:48:18', '2018-12-25 07:01:33'),
(7, 2, 'Super Deluxe Room', '', 'super-deluxe-room', 0, 1, '2018-12-20 09:49:24', '2018-12-25 07:01:53'),
(8, 2, 'Normal Room', '', 'normal-room', 2, 1, '2018-12-20 09:50:07', '2018-12-20 09:50:41'),
(9, 5, 'Restaurant', '', 'restaurant', 0, 1, '2018-12-20 10:51:35', '2018-12-25 07:01:49'),
(10, 5, 'Wifi', '', 'wifi', 1, 1, '2018-12-20 10:51:49', '2018-12-20 10:52:50'),
(11, 5, 'Swimming Pool', '', 'swimming-pool', 2, 1, '2018-12-20 10:52:05', '2018-12-20 10:52:52'),
(12, 5, 'Conference Room', '', 'conference-room', 3, 1, '2018-12-20 10:52:18', '2018-12-20 10:52:54'),
(13, 5, '24/7 Kitchen', '', '247-kitchen', 4, 1, '2018-12-20 10:52:34', '2018-12-20 10:53:01'),
(14, 0, 'Gallery', '', 'gallery', 6, 1, '2018-12-20 10:53:13', '2018-12-25 07:01:43');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text,
  `featured_facility` tinyint(1) NOT NULL DEFAULT '0',
  `feat_img` varchar(255) DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `accomodation_only` tinyint(1) NOT NULL DEFAULT '0',
  `detail_page` tinyint(1) NOT NULL DEFAULT '0',
  `about_page` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `slider` tinyint(1) NOT NULL DEFAULT '0',
  `slider_identifier` varchar(50) DEFAULT NULL,
  `meta_title` varchar(500) DEFAULT NULL,
  `meta_desc` text,
  `meta_keyword` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `title`, `slug`, `content`, `featured_facility`, `feat_img`, `ordering`, `accomodation_only`, `detail_page`, `about_page`, `status`, `slider`, `slider_identifier`, `meta_title`, `meta_desc`, `meta_keyword`, `created_at`, `updated_at`) VALUES
(7, 'Restaurant', 'restaurant', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet porro quae quasi temporibus qui facere enim earum voluptas ratione! Aspernatur nobis odio maiores velit et. Itaque, excepturi! Odit, magni reprehenderit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet porro quae quasi temporibus qui facere enim earum voluptas ratione! Aspernatur nobis odio maiores velit et. Itaque, excepturi! Odit, magni reprehenderit.</p>', 1, 'mBfXG-IMG_0974.jpg', 1, 0, 1, 0, 1, 1, 'ggAxT47fUG', 'Restaurant', 'Best hotel, pokhara, best of the best', 'Best Hotel in Pokhara', '2018-12-25 00:47:22', '2018-12-27 00:59:23'),
(8, 'Meeting Hall', 'meeting-hall', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet porro quae quasi temporibus qui facere enim earum voluptas ratione! Aspernatur nobis odio maiores velit et. Itaque, excepturi! Odit, magni reprehenderit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet porro quae quasi temporibus qui facere enim earum voluptas ratione! Aspernatur nobis odio maiores velit et. Itaque, excepturi! Odit, magni reprehenderit.</p>', 0, 'fD3rR-board-meeting.jpg', 2, 0, 1, 0, 1, 1, 'ggAxT47fUG', NULL, NULL, NULL, '2018-12-25 00:53:16', '2018-12-27 00:58:12'),
(9, 'Wifi', 'wifi', NULL, 0, '', 2, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, '2018-12-25 00:59:49', '2018-12-25 00:59:49'),
(12, 'The Barn', 'the-barn', 'cvxvcxv', 0, 'bwQ7A-admin-user.png', 2, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, '2018-12-25 03:45:16', '2018-12-26 10:53:49');

-- --------------------------------------------------------

--
-- Table structure for table `facility_features`
--

CREATE TABLE `facility_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_features`
--

INSERT INTO `facility_features` (`id`, `facility_id`, `title`, `created_at`, `updated_at`) VALUES
(3, 7, 'Fresh food', '2018-12-25 00:47:23', '2018-12-25 00:47:23'),
(4, 7, 'Table serve', '2018-12-25 00:47:23', '2018-12-25 00:47:23'),
(5, 7, '100% qualities', '2018-12-25 00:47:23', '2018-12-25 00:47:23'),
(6, 7, 'Happy Hours', '2018-12-25 00:47:23', '2018-12-25 00:47:23'),
(7, 8, 'Table serve', '2018-12-25 00:53:16', '2018-12-25 00:53:16'),
(8, 8, 'Projector', '2018-12-25 00:53:16', '2018-12-25 00:53:16'),
(9, 12, 'fgbfdgb', '2018-12-25 03:48:53', '2018-12-25 03:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `facility_sliders`
--

CREATE TABLE `facility_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_sliders`
--

INSERT INTO `facility_sliders` (`id`, `facility_id`, `image`, `ordering`, `created_at`, `updated_at`) VALUES
(8, 7, 'kYWDd-IMG_0974.jpg', 1, '2018-12-25 00:47:23', '2018-12-25 00:47:23'),
(9, 7, 'UNETf-IMG_0950.jpg', 2, '2018-12-25 00:47:23', '2018-12-25 00:47:23'),
(10, 8, 'rIOsW-board-meeting.jpg', 1, '2018-12-25 00:53:16', '2018-12-25 00:53:16'),
(11, 9, 'y2ftu-IMG_1394.jpg', 1, '2018-12-25 00:59:49', '2018-12-25 00:59:49'),
(14, 12, 'Qta3S-admin-user.png', 1, '2018-12-25 03:45:17', '2018-12-25 03:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contactno` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` text COLLATE utf8_unicode_ci,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `misc_javascript` text COLLATE utf8_unicode_ci,
  `footer_content` longtext COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `google_plus` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `iframe` text COLLATE utf8_unicode_ci,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `title`, `tagline`, `contactno`, `meta_title`, `meta_desc`, `meta_keyword`, `misc_javascript`, `footer_content`, `email`, `logo`, `logo2`, `favicon`, `created_at`, `updated_at`, `facebook`, `twitter`, `youtube`, `google_plus`, `instagram`, `iframe`, `contact_email`, `address`) VALUES
(15, 'Hotel Pokhara Goodwill', 'Best Hotel in Pokhara', '+977 987456321', 'Hotel Pokhara Goodwill', 'Hotel Pokhara Goodwill, best hotel', 'Hotel Pokhara Goodwill, best hotel', NULL, 'footer content here', 'admin@hotel.com', 'tBeeQ-logo.png', NULL, 'nCzw4-logo.png', '2018-12-14 22:41:18', '2018-12-25 08:56:45', '#', '#', '#', NULL, '#', 'iframe here', 'contact@hotel.com', 'Pokhara');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'save', 'bg-aqua', 'trans("history.backend.users.updated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","Administrator",1]}', '2018-12-15 07:14:44', '2018-12-15 07:14:44'),
(2, 1, 1, 1, 'lock', 'bg-blue', 'trans("history.backend.users.changed_password") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","Administrator",1]}', '2018-12-15 07:15:11', '2018-12-15 07:15:11'),
(3, 1, 1, 56, 'trash', 'bg-maroon', 'trans("history.backend.users.deleted") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","adsf",56]}', '2018-12-25 09:35:54', '2018-12-25 09:35:54'),
(4, 1, 1, 56, 'trash', 'bg-maroon', 'trans("history.backend.users.permanently_deleted") <strong>{user}</strong>', NULL, '2018-12-25 09:36:01', '2018-12-25 09:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2017-04-18 01:44:05', '2017-04-18 01:44:05'),
(2, 'Role', '2017-04-18 01:44:05', '2017-04-18 01:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_04_04_131153_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider` tinyint(1) NOT NULL DEFAULT '0',
  `slider_identifier` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `footer_menu` tinyint(1) NOT NULL DEFAULT '0',
  `footer_ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `meta_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` text COLLATE utf8_unicode_ci,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `top_content` longtext COLLATE utf8_unicode_ci,
  `bottom_content` longtext COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `slider`, `slider_identifier`, `footer_menu`, `footer_ordering`, `meta_title`, `meta_desc`, `meta_keyword`, `top_content`, `bottom_content`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Home', '/', 1, '8nlX8YhSub', 0, 0, 'home title', NULL, NULL, '{!! getCatProd([2, 1, 2], [''c-65'', ''c-62'', ''p-9'', ''c-61'', ''p-9'']) !!} {!!getAds(''2'')!!}{!! getCatProd([1, 2, 2], [''c-65'', ''c-62'', ''p-9'', ''c-61'', ''p-9'']) !!}{!!getAds(''1'')!!} {!! getCatProd([2, 2, 1], [''c-65'', ''c-62'', ''p-9'', ''c-61'', ''p-9'']) !!}', NULL, NULL, 1, '2017-05-25 20:20:55', '2018-12-26 10:41:41'),
(65, 'Terms And Conditions', 'terms-and-conditions', 0, NULL, 0, 0, NULL, NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL, NULL, 1, '2018-05-16 02:15:56', '2018-12-24 23:56:30'),
(69, 'About', 'about', 1, NULL, 0, 0, NULL, NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL, NULL, 1, '2018-03-15 02:05:55', '2018-12-26 10:41:30'),
(71, 'Accomodation', 'accomodation', 1, '8nlX8YhSub', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-12-25 02:11:33', '2018-12-25 02:11:33'),
(74, 'Facilities', 'facilities', 1, '8nlX8YhSub', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-12-25 05:44:22', '2018-12-25 05:44:22'),
(75, 'Career', 'career', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-12-26 11:09:15', '2018-12-26 11:09:15');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', 1, '2017-04-18 01:44:04', '2017-04-18 01:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `b_pattern` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `areacode` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `zone` varchar(100) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `faxcode` int(11) DEFAULT NULL,
  `faxareacode` int(11) DEFAULT NULL,
  `fax` bigint(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `b_pattern`, `fname`, `lname`, `code`, `areacode`, `phone`, `street`, `city`, `zone`, `country`, `gender`, `job_title`, `faxcode`, `faxareacode`, `fax`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Three', 'Hammers', 0, 0, '9898989898', NULL, NULL, NULL, NULL, '', '', 0, 0, 0, '', '2017-06-26 01:21:13', '2017-06-26 01:21:13'),
(2, 10, 0, 'test', 'lname', 0, 0, '9898989898', NULL, NULL, NULL, NULL, '', '', 0, 0, 0, '', '2017-06-26 01:21:13', '2017-06-26 01:21:13'),
(3, 11, 0, 'fname', 'lname', 0, 0, '989898988', NULL, NULL, NULL, NULL, '', '', 0, 0, 0, '', '2017-06-26 01:25:46', '2017-06-26 01:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2017-04-18 01:44:01', '2017-04-18 01:44:01'),
(2, 'Editor', 0, 2, '2017-04-18 01:44:01', '2018-12-15 06:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(53, 54, 2),
(54, 55, 2),
(57, 58, 2),
(60, 57, 2),
(61, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('LpBZ8D9r96ipTp8H0lThLrSWEplKhxihfkOV41PU', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0', 'ZXlKcGRpSTZJbVZ3VEdSclprSTJkSFp3T0VRcmVVOUhNbVZjTDFKM1BUMGlMQ0oyWVd4MVpTSTZJa1Z2Vld0cGJuSXJka1pWUlhGeU5tTXdSRU5wYm1vMlptTlVaR2hIWXpoTldHNU9lRXhrTXpaTWNWVTVPVlJDVEVwUVJXUmFhbHB6WmtGQllsd3ZNbnBzZVdnNVdtOVBlVE5LWTJNeFRIRjNSVXBVYURRMFRYZGlSakJFT0VwRlpGTlBhVEkyWldKVlNHVkxValZWUm5wa1lqaFBRbkZSVUZnNVprdGNMM3BJUzBZNWNVUk5YQzlRYkhCT1EyaHlVVlZwVlVaUWVVODJUMUp5Y1Zaa2FDdGhVMWcyU1c5U2IyNWFVME16ZFhoM1pEaFlNMnRtWnpaUVVtNXBXa0l5UWxJemIwMVhUbE1yVW5oTWJuSlpPR3BQT1ZsR2RucFFTMHhFUVdwT1IwcGlTemMzVjAxVk5VSkpTblJwVUUwellWbFJjMGtyYjBJeFZsd3ZVR2MyWTNoWVNrWmhlSEZ3ZVhKTVVHNWNMM01yTWpSMmVXOVlTakZaZG0xdmExWk5ablJ3YWpOc1MxQmhXblkyTTBJeFVHSmtiR0ZDZDJOaU5rTXpSVUprYlROUE5FRkZNVVF4UzBwV2VHNW9WRGRvWlVzemNHbGxSVk5XYVhGdU5VSmxTMnd6VURkTGNsbzBTMEZDV1hORlMyYzBjM04xWVdVeldtMXlkMjlJVkZJM1RuTXJPSGd4Tms1Q05Ib3hibTFCWWxWR2REVlVZa3g2YlUxVU1IRlpTVnd2ZEVWdVRVY3phMVJyWWpWMVpEaHlSRVkyVFdsdlZYRnlRelZTYTNGSVoyMWxWRFp6ZDJWbGR6WlVjV2RqWWpSM1NVZFVaMlJyVVZadk1tRkJQVDBpTENKdFlXTWlPaUppTURka05HUTBOMlZsTUdZeFptUXlOVGM1WldReVltVTVPRE0zTmpFM1pHUTNaRFEyTVRaa01qWmxZell3WVRJM1pURmtZamRpTURJNVl6STJaR0ZqSW4wPQ==', 1545893166);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `group_identifier` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `Slider_image` varchar(500) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `ordering` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `group_identifier`, `title`, `caption`, `Slider_image`, `link`, `ordering`, `created_at`, `updated_at`) VALUES
(101, '8nlX8YhSub', 'Main', NULL, 'images/slides/1545892811IMG_0905.jpg', NULL, 1, '2018-03-15 00:21:31', '2018-12-27 00:55:11'),
(102, '8nlX8YhSub', 'Main', NULL, 'images/slides/1545892826spa.jpg', NULL, 2, '2018-03-15 00:21:31', '2018-12-27 00:55:26'),
(103, '8nlX8YhSub', 'Main', NULL, 'images/slides/1545892846IMG_0958.jpg', NULL, 4, '2018-12-25 09:08:58', '2018-12-27 00:55:46'),
(104, '8nlX8YhSub', 'Main', NULL, 'images/slides/1545892834IMG_0974.jpg', NULL, 3, '2018-12-25 09:08:58', '2018-12-27 00:55:34'),
(105, '8nlX8YhSub', 'Main', NULL, 'images/slides/1545892860IMG_0950.jpg', NULL, 5, '2018-12-25 09:10:56', '2018-12-27 00:56:00'),
(107, 'ggAxT47fUG', 'Gallery', NULL, 'images/slides/1545892894board-meeting.jpg', NULL, NULL, '2018-12-26 10:06:45', '2018-12-27 00:56:34'),
(109, 'ggAxT47fUG', 'Gallery', NULL, 'images/slides/1545892905IMG_1194.jpg', NULL, NULL, '2018-12-26 10:16:54', '2018-12-27 00:56:45'),
(120, 'ggAxT47fUG', 'Gallery', NULL, 'images/slides/1545892920IMG_1394.jpg', NULL, NULL, '2018-12-27 00:57:00', '2018-12-27 00:57:00');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `static_blocks`
--

CREATE TABLE `static_blocks` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  `content` varchar(500) NOT NULL,
  `url` varchar(100) NOT NULL,
  `feature_image` varchar(100) NOT NULL,
  `position` varchar(50) NOT NULL,
  `s_order` int(5) NOT NULL,
  `bgcolor` varchar(10) NOT NULL,
  `bgimage` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_blocks`
--

INSERT INTO `static_blocks` (`id`, `title`, `identifier`, `page`, `content`, `url`, `feature_image`, `position`, `s_order`, `bgcolor`, `bgimage`, `status`, `created_at`, `updated_at`) VALUES
(2, 'test', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/14956979354-800x800.jpg', '1', 0, '#a43d6c', 'images/Static-blocks/1495618101cases.jpg', 1, '2017-05-22 06:37:24', '2017-05-25 01:53:55'),
(3, 'sdd', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/1495697737Start_Selling_enchanteur.jpg', '1', 0, '#123456', 'images/Static-blocks/1495455892scuffedstatic_dark1400x900.jpg', 1, '2017-05-22 06:39:52', '2017-05-25 01:50:37'),
(4, 'sdd', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/149569787112.jpg', '1', 0, '#123456', 'images/Static-blocks/1495455919scuffedstatic_dark1400x900.jpg', 1, '2017-05-22 06:40:19', '2017-05-25 01:52:51'),
(5, 'sdd', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/1495697812banner6-1.jpg', '1', 0, '#123456', 'images/Static-blocks/1495455934scuffedstatic_dark1400x900.jpg', 1, '2017-05-22 06:40:34', '2017-05-25 01:51:52'),
(15, 'block1', 'below block0', '53', 'block1', 'block1', 'images/Static-blocks/14956258264.png', '0', 3, '#021422', 'images/Static-blocks/14956258263.jpg', 1, '2017-05-24 05:52:06', '2017-05-24 05:52:06'),
(16, 'dd', 'dd', '53', 'dd', 'dd', 'images/Static-blocks/14956980137.jpg', '0', 0, 'primary', 'images/Static-blocks/14956286904.png', 0, '2017-05-24 06:39:50', '2017-05-25 01:55:13'),
(17, 'block', 'f', '53', 'ff', 'ff', 'images/Static-blocks/14956979956.jpg', '0', 0, 'primary', 'images/Static-blocks/14956286913.jpg', 0, '2017-05-24 06:39:51', '2017-05-25 05:04:08'),
(18, 'xcx', 'xcxc', '53', 'xcxc', 'xcx', 'images/Static-blocks/14956982412.jpg', '0', 0, 'primary', 'images/Static-blocks/1495628955cases.jpg', 0, '2017-05-24 06:44:15', '2017-05-25 01:59:01'),
(19, 'cxxc', 'xcxc', '53', 'xcxc', 'xcxc', 'images/Static-blocks/14956981823.jpg', '0', 0, 'primary', 'images/Static-blocks/1495629017scuffedstatic_dark1400x900.jpg', 0, '2017-05-24 06:45:17', '2017-05-25 01:58:02'),
(20, 'ddf', 'dds', '56', 'sdsd', 'sdsd', 'images/Static-blocks/1495697845banner6-2.jpg', '0', 0, 'primary', 'images/Static-blocks/1495697495cases.jpg', 0, '2017-05-25 01:46:35', '2017-05-25 01:52:25'),
(21, 'asdf', 'asdf', '1', 'asdfasdf', 'asdf', 'images/Static-blocks/1496143064$_57.jpg', '0', 0, '#212930', 'images/Static-blocks/1496139824DesktopSlider_NP_1.jpg', 1, '2017-05-30 04:38:44', '2017-05-30 05:32:44'),
(22, 'Top second', 'top-second', '1', 'asdfsdf', '#', 'images/Static-blocks/1496142101DesktopSlider_NP_2.jpg', '0', 2, '#556b7d', 'images/Static-blocks/1496142101DesktopSlider_NP_2.jpg', 1, '2017-05-30 05:16:41', '2017-05-30 05:16:41'),
(23, 'Top third', 'top-third', '1', 'asdfasdf', '#', 'images/Static-blocks/1496142250DesktopSlider_NP_3.jpg', '0', 3, 'primary', 'images/Static-blocks/1496142250DesktopSlider_NP_3.jpg', 1, '2017-05-30 05:19:10', '2017-05-30 05:19:10'),
(24, 'Bottom first', 'bottom-first', '1', 'asd asd', '#', 'images/Static-blocks/1496142309DesktopSlider_NP_2.jpg', '1', 0, 'primary', 'images/Static-blocks/1496142309DesktopSlider_NP_2.jpg', 1, '2017-05-30 05:20:09', '2017-05-30 05:36:42'),
(25, 'Bottom Second', 'bottom-second', '1', 'asdfsadfsdf', '#', 'images/Static-blocks/1496142384DesktopSlider_NP_3.jpg', '1', 2, 'primary', 'images/Static-blocks/1496142384DesktopSlider_NP_3.jpg', 1, '2017-05-30 05:21:24', '2017-05-30 05:21:24'),
(26, 'Bottom third', 'bottom-third', '1', 'asd asd fds', '#', 'images/Static-blocks/1496142384DesktopSlider_NP_1.jpg', '1', 3, 'primary', 'images/Static-blocks/1496142384DesktopSlider_NP_1.jpg', 1, '2017-05-30 05:21:24', '2017-05-30 05:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `review` text NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `address`, `review`, `ordering`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Yojan Shrestha', 'Lalitpur, Nepal', 'Without doubt one of the favorite hotels I stayed during my time in Pokhara! Upon arrival we were warmly welcomed by the staff and general manager. The staff was extremely friendly, the restaurant very flexible (appreciate this!) and the restaurant supervisor deserves a special thanks. We had two great evenings here, which obviously also added up to the experience.', 1, 1, '2018-12-15 11:49:33', '2018-12-26 11:00:16'),
(5, 'Sandy Joe', 'Australia', 'It’s a great place to stay.... Staff were friendly caring and pleasant, starting from the reception. Meals are very good and delicious...\r\nRooms are warm while outside is very cool. Well maintained hospital. I highly recommend the hotel for locals and foreigners. I would probably choose Orient hotel next time as I have experience warm welcomes of the staff, excellent service and pleasantness of hotel crew.', 2, 1, '2018-12-15 12:21:54', '2018-12-26 11:00:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cart_session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `cart_session_id`) VALUES
(1, 'Administrator', 'admin@admin.com', '$2y$10$bfLvqyWcb0PD3SeAkEglZ.8A/kUcphdygB9LvbSjskgtqDenbJcvC', 1, 'e8acf4eb44ff6a91db153be865e66d70', 1, 'jeV7hYbsPrBgPbyDPJoxaA5iHQfWoQRrj0xwFNszyTGQP1oXUHyrCha5sNED', '2017-04-18 01:44:00', '2018-12-15 07:15:11', NULL, NULL),
(54, 'Editor User', 'yojansh50@gmail.com', '$2y$10$AzKTn8fXZ3DyHLPEtKQV5uFgc9gNn/HVx/61T6X5vMDIoYbPz7RUq', 1, 'b47508733ec7476a07cd5330083664ef', 1, 'dpolZwMeGK6Ebq9btvQxpCpJHXYwYohlz6olV5y5aM38KQXrWQ7ni969iFoQ', '2018-12-15 06:25:00', '2018-12-15 06:25:00', NULL, NULL),
(57, 'test3', 'test3@gmail.com', '$2y$10$KfVNuaenQfl1LQb8nDOYCeVKJpjWsyJb9JMTCsnznkhNE/hKaYuy.', 1, '5afab2a4eb0d17adadf10cbac8d9c997', 1, 'dAAldLC6CTGNqIL4W8b4ODW0CxUXqyUE08zRvuf4moyUsrf1TmKZ7U4nWmm5', '2018-12-15 06:29:39', '2018-12-15 07:07:18', NULL, NULL),
(58, 'test4', 'test4@gmail.com', '$2y$10$EmesF5rmM24FYj1Oy9TpN.4c4sjCPH0nkNODdJlXh.OIXjR9SRSzq', 1, '36353b255aa9f0c1ca18ed9c7f854d2f', 1, 'mF0uvvj5oDfUkvdEHQQzKE0DUJTqcVRnTSycqrK0y40Zra91FZzR96Ijv3d5', '2018-12-15 06:50:28', '2018-12-15 06:50:28', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomodations`
--
ALTER TABLE `accomodations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accomodation_facility`
--
ALTER TABLE `accomodation_facility`
  ADD KEY `facility_id` (`facility_id`),
  ADD KEY `accomodation_id` (`accomodation_id`);

--
-- Indexes for table `accomodation_sliders`
--
ALTER TABLE `accomodation_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accomodation_id` (`accomodation_id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_features`
--
ALTER TABLE `facility_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Indexes for table `facility_sliders`
--
ALTER TABLE `facility_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `static_blocks`
--
ALTER TABLE `static_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomodations`
--
ALTER TABLE `accomodations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `accomodation_sliders`
--
ALTER TABLE `accomodation_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `facility_features`
--
ALTER TABLE `facility_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `facility_sliders`
--
ALTER TABLE `facility_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `static_blocks`
--
ALTER TABLE `static_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `accomodation_facility`
--
ALTER TABLE `accomodation_facility`
  ADD CONSTRAINT `accomodation_facility_ibfk_1` FOREIGN KEY (`accomodation_id`) REFERENCES `accomodations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `accomodation_facility_ibfk_2` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `accomodation_sliders`
--
ALTER TABLE `accomodation_sliders`
  ADD CONSTRAINT `accomodation_sliders_ibfk_1` FOREIGN KEY (`accomodation_id`) REFERENCES `accomodations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `facility_features`
--
ALTER TABLE `facility_features`
  ADD CONSTRAINT `facility_features_ibfk_1` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `facility_sliders`
--
ALTER TABLE `facility_sliders`
  ADD CONSTRAINT `facility_sliders_ibfk_1` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2018 at 07:59 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `goodwill`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `cat_type` varchar(50) DEFAULT NULL,
  `description` longtext,
  `homepage_display` tinyint(1) NOT NULL DEFAULT '0',
  `max_price` int(10) DEFAULT NULL,
  `feat_img` varchar(255) DEFAULT NULL,
  `second_img` varchar(255) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `total_views` int(10) DEFAULT NULL,
  `meta_title` varchar(500) DEFAULT NULL,
  `meta_keyword` text,
  `meta_desc` text,
  `order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `parent_id`, `title`, `label`, `url`, `cat_type`, `description`, `homepage_display`, `max_price`, `feat_img`, `second_img`, `img_url`, `total_views`, `meta_title`, `meta_keyword`, `meta_desc`, `order`, `status`, `created_at`, `updated_at`) VALUES
(61, 0, 'Women', '', 'women', 'more', NULL, 0, NULL, '3Svo7-home~box~5-1.png', '', NULL, NULL, NULL, NULL, NULL, 0, 1, '0000-00-00 00:00:00', '2018-12-15 08:22:05'),
(62, 0, 'Men', '', 'men', 'more', NULL, 0, NULL, 'DGXog-home~box~2-1.png', '', NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-03-12 06:56:03', '2018-12-15 08:22:05'),
(63, 0, 'Baby & Kids', '', 'bady-kids', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 2, 1, '2018-03-12 06:56:27', '2018-12-15 11:25:53'),
(64, 61, 'Ethic Wear', '', 'ethic-wear', 'more', NULL, 0, NULL, '', '', NULL, 5, NULL, NULL, NULL, 0, 1, '2018-03-12 06:57:35', '2018-03-28 09:23:54'),
(65, 64, 'New Arrivals', '', 'new-arrivals', 'more', 'ssnpwears is a marketplace for designer merchandise created by fantastic independent artists from all over the world. We connect art with lifestyle by enabling you to shop directly from creators. Browse and shop thousands of inspring designs accross a range of quality products.', 0, NULL, 'je0Q4-thumb-img.jpg', '', NULL, 5, 'Women new arrivals', NULL, NULL, 0, 1, '2018-03-12 06:57:51', '2018-05-04 10:50:15'),
(66, 64, 'Party wear kurta suruwal', '', 'party-wear-kurta-suruwal', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-03-12 06:58:36', '2018-03-19 07:54:43'),
(67, 64, 'Anarkali Kurta Surwal', '', 'anarkali-kurta-surwal', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 2, 1, '2018-03-14 11:47:09', '2018-03-14 11:48:48'),
(68, 64, 'Sarees', '', 'sarees', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 3, 1, '2018-03-14 11:47:34', '2018-03-14 11:49:04'),
(69, 61, 'Western Wear', '', 'western-wear', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 2, 1, '2018-03-14 11:48:03', '2018-03-14 11:49:02'),
(70, 69, 'T-shirts', '', 't-shirts', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, 1, '2018-03-14 11:48:36', '2018-03-14 11:49:00'),
(71, 69, 'Leggings', '', 'leggings', 'more', NULL, 0, NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, 1, '2018-03-14 11:49:19', '2018-03-14 11:49:51'),
(72, 0, 'Accommod', '', 'accom', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, '2018-12-15 08:14:48', '2018-12-15 08:25:04'),
(73, 0, 'asdf', '', 'asdf', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 0, '2018-12-15 08:25:16', '2018-12-15 08:25:16'),
(74, 0, 'asdfasdf', '', 'asdfasdf', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, '2018-12-15 11:15:13', '2018-12-15 11:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` text,
  `featured_facility` tinyint(1) NOT NULL DEFAULT '0',
  `feat_img` varchar(255) DEFAULT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `accomodation_only` tinyint(1) NOT NULL DEFAULT '0',
  `detail_page` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `title`, `slug`, `content`, `featured_facility`, `feat_img`, `ordering`, `accomodation_only`, `detail_page`, `status`, `created_at`, `updated_at`) VALUES
(2, 'title2d', 'title2d', 'contetn hehhe', 0, 'yHRe0-38290550_1973955322665347_5298586696491728896_n.jpg', 1, 1, 0, 0, '2018-12-18 06:00:21', '2018-12-18 06:00:21'),
(3, 'title3', 'title3', NULL, 0, 'qpXSY-38290550_1973955322665347_5298586696491728896_n.jpg', 2, 0, 1, 0, '2018-12-18 06:06:05', '2018-12-18 06:06:05'),
(6, 'title', 'title-1', NULL, 0, 'LBYhn-images.jpg', 1, 0, 1, 0, '2018-12-18 06:20:21', '2018-12-20 00:51:56'),
(7, 'Testing Facility', 'testing-facility', 'contetnt hehehhe', 1, 'pwmI1-images.jpg', 1, 0, 1, 1, '2018-12-20 01:03:25', '2018-12-20 01:04:08');

-- --------------------------------------------------------

--
-- Table structure for table `facility_features`
--

CREATE TABLE `facility_features` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_features`
--

INSERT INTO `facility_features` (`id`, `facility_id`, `title`, `created_at`, `updated_at`) VALUES
(3, 2, 'feature1', '2018-12-18 06:00:21', '2018-12-18 06:00:21'),
(4, 2, 'feature2', '2018-12-18 06:00:21', '2018-12-18 06:00:21'),
(5, 3, 'adf', '2018-12-18 06:06:05', '2018-12-18 06:06:05'),
(6, 3, 'ewrewr', '2018-12-18 06:06:05', '2018-12-18 06:06:05'),
(10, 6, 'asdf', '2018-12-18 06:20:21', '2018-12-20 00:05:14'),
(11, 6, 'asdf2', '2018-12-18 06:20:21', '2018-12-20 00:05:14'),
(14, 7, 'Feat1', '2018-12-20 01:03:25', '2018-12-20 01:03:25'),
(15, 7, 'feat2', '2018-12-20 01:03:25', '2018-12-20 01:03:25');

-- --------------------------------------------------------

--
-- Table structure for table `facility_sliders`
--

CREATE TABLE `facility_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `facility_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility_sliders`
--

INSERT INTO `facility_sliders` (`id`, `facility_id`, `image`, `ordering`, `created_at`, `updated_at`) VALUES
(4, 6, 'WDHKb-maxresdefault.jpg', 4, '2018-12-20 00:48:06', '2018-12-20 00:51:15'),
(6, 7, 'FwbGE-maxresdefault.jpg', 2, '2018-12-20 01:03:25', '2018-12-20 01:03:25'),
(7, 7, 'qp6Dn-Screenshot_2017-12-20-04-53-00.png', 2, '2018-12-20 01:03:25', '2018-12-20 01:03:25');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contactno` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` text COLLATE utf8_unicode_ci,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `misc_javascript` text COLLATE utf8_unicode_ci,
  `footer_content` longtext COLLATE utf8_unicode_ci,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `google_plus` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT '#',
  `iframe` text COLLATE utf8_unicode_ci,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `title`, `tagline`, `contactno`, `meta_title`, `meta_desc`, `meta_keyword`, `misc_javascript`, `footer_content`, `email`, `logo`, `logo2`, `favicon`, `created_at`, `updated_at`, `facebook`, `twitter`, `youtube`, `google_plus`, `instagram`, `iframe`, `contact_email`, `address`) VALUES
(15, 'Hotel Pokhara Goodwill', 'Best Hotel in Pokhara', '+977 987456321', 'Hotel Pokhara Goodwill', 'Hotel Pokhara Goodwill, best hotel desc', 'Hotel Pokhara Goodwill, best hotel', NULL, 'footer content here', 'admin@hotel.com', 'n1li2-goodwill.png', NULL, '6k97E-goodwill.png', '2018-12-14 22:41:18', '2018-12-14 22:43:26', '#', '#', '#', NULL, '#', 'iframe here', 'contact@hotel.com', 'Pokhara');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `type_id`, `user_id`, `entity_id`, `icon`, `class`, `text`, `assets`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'save', 'bg-aqua', 'trans("history.backend.users.updated") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","Administrator",1]}', '2018-12-15 07:14:44', '2018-12-15 07:14:44'),
(2, 1, 1, 1, 'lock', 'bg-blue', 'trans("history.backend.users.changed_password") <strong>{user}</strong>', '{"user_link":["admin.access.user.show","Administrator",1]}', '2018-12-15 07:15:11', '2018-12-15 07:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2017-04-18 01:44:05', '2017-04-18 01:44:05'),
(2, 'Role', '2017-04-18 01:44:05', '2017-04-18 01:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_04_04_131153_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slider` tinyint(1) NOT NULL DEFAULT '0',
  `slider_identifier` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_desc` text COLLATE utf8_unicode_ci,
  `meta_keyword` text COLLATE utf8_unicode_ci,
  `top_content` longtext COLLATE utf8_unicode_ci,
  `bottom_content` longtext COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `slider`, `slider_identifier`, `meta_title`, `meta_desc`, `meta_keyword`, `top_content`, `bottom_content`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Home', 'home', 1, '8nlX8YhSub', 'home title', NULL, NULL, '{!! getCatProd([2, 1, 2], [''c-65'', ''c-62'', ''p-9'', ''c-61'', ''p-9'']) !!} {!!getAds(''2'')!!}{!! getCatProd([1, 2, 2], [''c-65'', ''c-62'', ''p-9'', ''c-61'', ''p-9'']) !!}{!!getAds(''1'')!!} {!! getCatProd([2, 2, 1], [''c-65'', ''c-62'', ''p-9'', ''c-61'', ''p-9'']) !!}', NULL, NULL, 1, '2017-05-25 20:20:55', '2018-05-30 06:02:54'),
(64, 'About Us', 'about-us', 1, '8nlX8YhSub', NULL, NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL, NULL, 1, '2018-03-15 02:05:55', '2018-05-16 02:15:03'),
(65, 'Terms And Conditions', 'terms-conditions', 0, NULL, NULL, NULL, NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL, NULL, 1, '2018-05-16 02:15:56', '2018-05-16 02:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', 1, '2017-04-18 01:44:04', '2017-04-18 01:44:04');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `b_pattern` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `areacode` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `zone` varchar(100) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `faxcode` int(11) DEFAULT NULL,
  `faxareacode` int(11) DEFAULT NULL,
  `fax` bigint(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `b_pattern`, `fname`, `lname`, `code`, `areacode`, `phone`, `street`, `city`, `zone`, `country`, `gender`, `job_title`, `faxcode`, `faxareacode`, `fax`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Three', 'Hammers', 0, 0, '9898989898', NULL, NULL, NULL, NULL, '', '', 0, 0, 0, '', '2017-06-26 01:21:13', '2017-06-26 01:21:13'),
(2, 10, 0, 'test', 'lname', 0, 0, '9898989898', NULL, NULL, NULL, NULL, '', '', 0, 0, 0, '', '2017-06-26 01:21:13', '2017-06-26 01:21:13'),
(3, 11, 0, 'fname', 'lname', 0, 0, '989898988', NULL, NULL, NULL, NULL, '', '', 0, 0, 0, '', '2017-06-26 01:25:46', '2017-06-26 01:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2017-04-18 01:44:01', '2017-04-18 01:44:01'),
(2, 'Editor', 0, 2, '2017-04-18 01:44:01', '2018-12-15 06:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(53, 54, 2),
(54, 55, 2),
(57, 58, 2),
(60, 57, 2),
(61, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('Kw4gYCknLyEEwuLXc6YSCDdzR7TLQMTacVz6oSjx', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0', 'ZXlKcGRpSTZJbmRUTjIxVWVVRlhTblpSYldOMGJIcGtkV3RaYjJjOVBTSXNJblpoYkhWbElqb2lYQzlqYWtobVVGbHpObE5jTDJwWVowdFlhWFI1TjFoaWNrbFVaVTVqWTNJMmVuRkxOa3M1ZEdoeldIRkZhRU5vU2taRFNtNXNVRWhhYlhwV01tcFlTRXhrUjJscWRXNTVOMEl5VlRSd1UzaHFjVzlCWjFGdVJYQnFSMDR5T1RGVU1ERk1kbU5OWmtWNGQyTlRRamswTVdrclFtVTBOWGw0ZFZKcVdHeElkRVZ0TmxCSGRIQjRZMnRhTml0cFpISTFOMGhWVHl0Q1lYQkJXbEp6WTJsWk5YVk1OVXRuY1V4TlJ6Tk5iMFZpTUZoYWIyRmxjSGhUZG05VU5FeG5abUZqU0Z3dlRIZEZlSFIxV0VZemJFaFdNall3WVVGdmR6Z3lNV0Y2WjJKUlVsVkdPRTVaVjFwcFZUQjJWRFZjTDBsR1RrTnhTVmRDWTNaSmFVWkNZMWwyUVU5c1ZqTTVWbUpyUjA5V01XUmFkRTlEWlN0MFZuTkhka2c1VUhkbVRqTndTMnBITUZrMFdGa3hiVmcwUldWSVZESXhTR2RSWkRaMGIzTkhYQzlxVm1GTFNHMDRWa0puVWxSdU5VVkNiV3g2UzI1cVJuRnNNa0ZqTlRoYVFXZFBTblZKUTNGWVp6SnZUbEpYTjBKVloyUlhTR1JIVjNkd1JHVnZPWGczUjFsWFVtRnRkazh5ZEdsRVNrbEdiVEp6UkU1MFUyVkZiSGhsZDFSa1VXVnNNekZTYjFSaWNsTktNelZXTkZsdVNuZFNaRFpUYzJoWWRXeG5NakZ3YzNaMFRWbDNRVkpzSWl3aWJXRmpJam9pWVdJNU4yVXdaVGs0WXpnME5EQm1aRFkzWlRnMU4yUm1aalF5T1RFelpHSTNNbVExT0RNeFltSTNPRFZrWm1KaU5qWmlOekkzTmpZek5XVXpNamszTWlKOQ==', 1545289074);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `group_identifier` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `caption` varchar(500) DEFAULT NULL,
  `Slider_image` varchar(500) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `ordering` int(10) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `group_identifier`, `title`, `caption`, `Slider_image`, `link`, `ordering`, `created_at`, `updated_at`) VALUES
(101, '8nlX8YhSub', 'Home', NULL, 'images/slides/1521093991slider-1.jpg', NULL, 0, '2018-03-15 00:21:31', '2018-03-15 00:21:31'),
(102, '8nlX8YhSub', 'Home', NULL, 'images/slides/1521093991slider-2.jpg', NULL, 0, '2018-03-15 00:21:31', '2018-03-15 00:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `static_blocks`
--

CREATE TABLE `static_blocks` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `page` varchar(100) NOT NULL,
  `content` varchar(500) NOT NULL,
  `url` varchar(100) NOT NULL,
  `feature_image` varchar(100) NOT NULL,
  `position` varchar(50) NOT NULL,
  `s_order` int(5) NOT NULL,
  `bgcolor` varchar(10) NOT NULL,
  `bgimage` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `static_blocks`
--

INSERT INTO `static_blocks` (`id`, `title`, `identifier`, `page`, `content`, `url`, `feature_image`, `position`, `s_order`, `bgcolor`, `bgimage`, `status`, `created_at`, `updated_at`) VALUES
(2, 'test', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/14956979354-800x800.jpg', '1', 0, '#a43d6c', 'images/Static-blocks/1495618101cases.jpg', 1, '2017-05-22 06:37:24', '2017-05-25 01:53:55'),
(3, 'sdd', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/1495697737Start_Selling_enchanteur.jpg', '1', 0, '#123456', 'images/Static-blocks/1495455892scuffedstatic_dark1400x900.jpg', 1, '2017-05-22 06:39:52', '2017-05-25 01:50:37'),
(4, 'sdd', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/149569787112.jpg', '1', 0, '#123456', 'images/Static-blocks/1495455919scuffedstatic_dark1400x900.jpg', 1, '2017-05-22 06:40:19', '2017-05-25 01:52:51'),
(5, 'sdd', 'dd', '56', 'dsd', 'ddd', 'images/Static-blocks/1495697812banner6-1.jpg', '1', 0, '#123456', 'images/Static-blocks/1495455934scuffedstatic_dark1400x900.jpg', 1, '2017-05-22 06:40:34', '2017-05-25 01:51:52'),
(15, 'block1', 'below block0', '53', 'block1', 'block1', 'images/Static-blocks/14956258264.png', '0', 3, '#021422', 'images/Static-blocks/14956258263.jpg', 1, '2017-05-24 05:52:06', '2017-05-24 05:52:06'),
(16, 'dd', 'dd', '53', 'dd', 'dd', 'images/Static-blocks/14956980137.jpg', '0', 0, 'primary', 'images/Static-blocks/14956286904.png', 0, '2017-05-24 06:39:50', '2017-05-25 01:55:13'),
(17, 'block', 'f', '53', 'ff', 'ff', 'images/Static-blocks/14956979956.jpg', '0', 0, 'primary', 'images/Static-blocks/14956286913.jpg', 0, '2017-05-24 06:39:51', '2017-05-25 05:04:08'),
(18, 'xcx', 'xcxc', '53', 'xcxc', 'xcx', 'images/Static-blocks/14956982412.jpg', '0', 0, 'primary', 'images/Static-blocks/1495628955cases.jpg', 0, '2017-05-24 06:44:15', '2017-05-25 01:59:01'),
(19, 'cxxc', 'xcxc', '53', 'xcxc', 'xcxc', 'images/Static-blocks/14956981823.jpg', '0', 0, 'primary', 'images/Static-blocks/1495629017scuffedstatic_dark1400x900.jpg', 0, '2017-05-24 06:45:17', '2017-05-25 01:58:02'),
(20, 'ddf', 'dds', '56', 'sdsd', 'sdsd', 'images/Static-blocks/1495697845banner6-2.jpg', '0', 0, 'primary', 'images/Static-blocks/1495697495cases.jpg', 0, '2017-05-25 01:46:35', '2017-05-25 01:52:25'),
(21, 'asdf', 'asdf', '1', 'asdfasdf', 'asdf', 'images/Static-blocks/1496143064$_57.jpg', '0', 0, '#212930', 'images/Static-blocks/1496139824DesktopSlider_NP_1.jpg', 1, '2017-05-30 04:38:44', '2017-05-30 05:32:44'),
(22, 'Top second', 'top-second', '1', 'asdfsdf', '#', 'images/Static-blocks/1496142101DesktopSlider_NP_2.jpg', '0', 2, '#556b7d', 'images/Static-blocks/1496142101DesktopSlider_NP_2.jpg', 1, '2017-05-30 05:16:41', '2017-05-30 05:16:41'),
(23, 'Top third', 'top-third', '1', 'asdfasdf', '#', 'images/Static-blocks/1496142250DesktopSlider_NP_3.jpg', '0', 3, 'primary', 'images/Static-blocks/1496142250DesktopSlider_NP_3.jpg', 1, '2017-05-30 05:19:10', '2017-05-30 05:19:10'),
(24, 'Bottom first', 'bottom-first', '1', 'asd asd', '#', 'images/Static-blocks/1496142309DesktopSlider_NP_2.jpg', '1', 0, 'primary', 'images/Static-blocks/1496142309DesktopSlider_NP_2.jpg', 1, '2017-05-30 05:20:09', '2017-05-30 05:36:42'),
(25, 'Bottom Second', 'bottom-second', '1', 'asdfsadfsdf', '#', 'images/Static-blocks/1496142384DesktopSlider_NP_3.jpg', '1', 2, 'primary', 'images/Static-blocks/1496142384DesktopSlider_NP_3.jpg', 1, '2017-05-30 05:21:24', '2017-05-30 05:21:24'),
(26, 'Bottom third', 'bottom-third', '1', 'asd asd fds', '#', 'images/Static-blocks/1496142384DesktopSlider_NP_1.jpg', '1', 3, 'primary', 'images/Static-blocks/1496142384DesktopSlider_NP_1.jpg', 1, '2017-05-30 05:21:24', '2017-05-30 05:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `review` text NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `address`, `review`, `ordering`, `status`, `created_at`, `updated_at`) VALUES
(1, 'name1', 'address1', 'review1', 1, 1, '2018-12-15 11:49:33', '2018-12-15 12:22:30'),
(5, 'iuiu', 'iui', 'iuuiu', 2, 0, '2018-12-15 12:21:54', '2018-12-15 12:21:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `cart_session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `cart_session_id`) VALUES
(1, 'Administrator', 'admin@admin.com', '$2y$10$bfLvqyWcb0PD3SeAkEglZ.8A/kUcphdygB9LvbSjskgtqDenbJcvC', 1, 'e8acf4eb44ff6a91db153be865e66d70', 1, 'jeV7hYbsPrBgPbyDPJoxaA5iHQfWoQRrj0xwFNszyTGQP1oXUHyrCha5sNED', '2017-04-18 01:44:00', '2018-12-15 07:15:11', NULL, NULL),
(54, 'Editor User', 'yojansh50@gmail.com', '$2y$10$AzKTn8fXZ3DyHLPEtKQV5uFgc9gNn/HVx/61T6X5vMDIoYbPz7RUq', 1, 'b47508733ec7476a07cd5330083664ef', 1, 'dpolZwMeGK6Ebq9btvQxpCpJHXYwYohlz6olV5y5aM38KQXrWQ7ni969iFoQ', '2018-12-15 06:25:00', '2018-12-15 06:25:00', NULL, NULL),
(56, 'adsf', 'test2@gmail.com', '$2y$10$RPJoaxiWbVs6CVyToY0klOwg/Pr/rD/MzstYcDpG1CeVFOaZopYj6', 1, '737156b86280fffc2945520c67a4eef8', 0, NULL, '2018-12-15 06:28:58', '2018-12-15 06:28:58', NULL, NULL),
(57, 'test3', 'test3@gmail.com', '$2y$10$KfVNuaenQfl1LQb8nDOYCeVKJpjWsyJb9JMTCsnznkhNE/hKaYuy.', 1, '5afab2a4eb0d17adadf10cbac8d9c997', 1, 'dAAldLC6CTGNqIL4W8b4ODW0CxUXqyUE08zRvuf4moyUsrf1TmKZ7U4nWmm5', '2018-12-15 06:29:39', '2018-12-15 07:07:18', NULL, NULL),
(58, 'test4', 'test4@gmail.com', '$2y$10$EmesF5rmM24FYj1Oy9TpN.4c4sjCPH0nkNODdJlXh.OIXjR9SRSzq', 1, '36353b255aa9f0c1ca18ed9c7f854d2f', 1, 'mF0uvvj5oDfUkvdEHQQzKE0DUJTqcVRnTSycqrK0y40Zra91FZzR96Ijv3d5', '2018-12-15 06:50:28', '2018-12-15 06:50:28', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_features`
--
ALTER TABLE `facility_features`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Indexes for table `facility_sliders`
--
ALTER TABLE `facility_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `static_blocks`
--
ALTER TABLE `static_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `facility_features`
--
ALTER TABLE `facility_features`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `facility_sliders`
--
ALTER TABLE `facility_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `static_blocks`
--
ALTER TABLE `static_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `facility_features`
--
ALTER TABLE `facility_features`
  ADD CONSTRAINT `facility_features_ibfk_1` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `facility_sliders`
--
ALTER TABLE `facility_sliders`
  ADD CONSTRAINT `facility_sliders_ibfk_1` FOREIGN KEY (`facility_id`) REFERENCES `facilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
protected $table = 'categorys';

public static function getRelatedSearchLimit(){
	return  5;
}

	// Recursive function that builds the menu from an array or object of items
	// In a perfect world some parts of this function would be in a custom Macro or a View
	public function buildMenu($menu, $parentid = 0) 
	{ 
	  $result = null;
	  foreach ($menu as $item) 
	    if ($item->parent_id == $parentid) { 
	    	if ($item->status == 1) {
	    		$result .= "<li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
		        <div class='dd-handle nested-list-handle'>
		        	<span class='glyphicon glyphicon-move'></span>
		        </div>
		        <div class='nested-list-content'>{$item->title} 
		        	<div class='pull-right'>
			          <span class='label label-success'>Enabled</span><a href='".url("admin/menu/edit/{$item->id}")."'> Edit</a> |
			          <a href='#' class='delete_toggle' rel='{$item->id}'>Delete</a>
		        	</div>
		        </div>".$this->buildMenu($menu, $item->id) . "</li>"; 
	    	}else{
	    		$result .= "<li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
		        <div class='dd-handle nested-list-handle'>
		        	<span class='glyphicon glyphicon-move'></span>
		        </div>
		        <div class='nested-list-content'>{$item->title} 
		        	<div class='pull-right'>
			          <span class='label label-danger'>Disabled</span><a href='".url("admin/menu/edit/{$item->id}")."'> Edit</a> |
			          <a href='#' class='delete_toggle' rel='{$item->id}'>Delete</a>
		        	</div>
		        </div>".$this->buildMenu($menu, $item->id) . "</li>"; 
	    	}
 
	    } 
	  return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null; 
	} 

	// Getter for the HTML menu builder
	public function getHTML($items)
	{
		return $this->buildMenu($items);
	}
	public function scopeRoot($query)
    {
        return $query->where('parent_id', 0); //or null, not sure how you are handling
    }
	public function children()
	{
		return $this->hasMany('App\Models\Category', 'parent_id');
	}
	public function scopeActive($query)
    {
        return $query->where('status', 1); //or null, not sure how you are handling
    }
    public function activeChildren()
	{
		return $this->hasMany('App\Models\Category', 'parent_id')->active();
	}
	public function scopeOrder($query)
    {
        return $query->orderBy('order','ASC');
    }
  	
}

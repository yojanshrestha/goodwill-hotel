<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'testimonials';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return "<label class='label label-success'>".'Enabled'.'</label>';
        }

        return "<label class='label label-danger'>".'Disabled'.'</label>';
    }
    public function scopeActive($query)
    {
        return $query->where('status', 1); //or null, not sure how you are handling
    }
    public function scopeOrder($query)
    {
        return $query->orderBy('ordering','ASC');
    }
}

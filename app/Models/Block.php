<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blocks';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function scopeOrder($query){
        $query->orderBy('ordering');
    }

}

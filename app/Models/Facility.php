<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'facilities';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return "<label class='label label-success'>".'Enabled'.'</label>';
        }

        return "<label class='label label-danger'>".'Disabled'.'</label>';
    }

    /**
     * @return string
     */
    public function getAccomodationOnlyLabelAttribute()
    {
        if ($this->accomodation_only == 1) {
            return "<label class='label label-danger'>".'Accomodation only'.'</label>';
        }

        return "<label class='label label-success'>".'All'.'</label>';
    }

    /**
     * @return string
     */
    public function getDetailPageLabelAttribute()
    {
        if ($this->detail_page == 1) {
            return "<label class='label label-success'>".'Enabled'.'</label>';
        }

        return "<label class='label label-danger'>".'Disabled'.'</label>';
    }

    /**
     * @return string
     */
    public function getFeaturedFacilityLabelAttribute()
    {
        if ($this->featured_facility == 1) {
            return "<label class='label label-success'>".'Yes'.'</label>';
        }

        return "<label class='label label-danger'>".'No'.'</label>';
    }

    public function features(){
        return $this->hasMany('\App\Models\FacilityFeature', 'facility_id');
    }

    public function sliders(){
        return $this->hasMany('\App\Models\FacilitySlider', 'facility_id')->order();
    }

    public function scopeOrder($query){
        $query->orderBy('ordering');
    }

    public function scopeActive($query){
        $query->where('status', 1);
    }

    public function scopeFeatured($query){
        $query->where('featured_facility', 1);
    }

    public function scopeDetailPage($query){
        $query->where('detail_page', 1);
    }

}

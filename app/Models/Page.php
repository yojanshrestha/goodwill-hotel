<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return "<label class='label label-success'>".'Enabled'.'</label>';
        }
        return "<label class='label label-danger'>".'Disabled'.'</label>';
    }

    public function getFooterMenuLabelAttribute()
    {
        if ($this->footer_menu == 1) {
            return "<label class='label label-success'>".'Yes'.'</label>';
        }

        return "<label class='label label-danger'>".'No'.'</label>';
    }

    public function scopeActive($query) {
        return $query->where('status', 1);
    }

    public static function getTemplateArray(){
        return  [
            'default'=>'Default',
            'about'=>'About',
            'contact'=>'Contact',
            'accomodation'=>'Accomodation',
            'gallery'=>'Gallery',
            'facility'=>'Facility'
        ];
    }

}

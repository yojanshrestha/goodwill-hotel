<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccomodationSlider extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accomodation_sliders';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function scopeOrder($query){
        $query->orderBy('ordering');
    }


}

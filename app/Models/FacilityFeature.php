<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilityFeature extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'facility_features';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


}

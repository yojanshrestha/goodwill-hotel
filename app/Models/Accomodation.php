<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accomodations';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return "<label class='label label-success'>".'Enabled'.'</label>';
        }

        return "<label class='label label-danger'>".'Disabled'.'</label>';
    }

    public function sliders(){
        return $this->hasMany('\App\Models\AccomodationSlider', 'accomodation_id')->order();
    }

    public function facilities(){
        return $this->belongsToMany('\App\Models\Facility', 'accomodation_facility', 'accomodation_id', 'facility_id');
    }

    public function scopeOrder($query){
        $query->orderBy('ordering');
    }

    public function scopeActive($query){
        $query->where('status', 1);
    }

}

<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Testimonial;
use App\Models\Page;
use App\Models\Slide;
use App\Models\Accomodation;
use App\Models\Facility;
use Auth;
use App\Models\Block;
use Mail;

class FrontendController extends Controller
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function index()
    {
        $page           = Page::where('slug', '/')->active()->first();
        if (empty($page)) {
            $page       = Page::where('id', 1)->active()->first();
        }
        if (empty($page)) {
            abort(404);
        }
        $slides         = Slide::where('group_identifier', $page->slider_identifier)->order()->get();
    	$testimonials   = Testimonial::active()->order()->get();
        $blocks         = Block::order()->get();

    	return view('frontend/index', compact('slides', 'testimonials', 'page', 'blocks'));	
    }

    public function page($slug)
    {
        $page               = Page::where('slug', $slug)->active()->where('id', '!=', 1)->first();
        $accomodations      = Accomodation::select('id', 'title', 'slug', 'feat_img')->active()->order()->get();
        $facilities         = Facility::select('id', 'title', 'content', 'feat_img')->where('about_page', 1)->active()->order()->get();
        $feat_facility      = Facility::select('id', 'title', 'content')->featured()->active()->order()->detailpage()->first();

        if($feat_facility){
            $other_facilities   = Facility::select('id', 'slug', 'title', 'feat_img')->active()->order()->detailpage()->where('id', '!=', $feat_facility->id)->get();
        }else{
            $other_facilities   = Facility::select('id', 'slug', 'title', 'feat_img')->active()->order()->detailpage()->get();
        }

        if($page){
            if($page->slider == 1 && $page->slider_identifier != '')
            { 
                $slides = Slide::where('group_identifier', $page->slider_identifier)->order()->get();
            } else {
                $slides = array();
            }

            if($page->template == 'about') {
                return view('frontend/pages/about',compact('slides', 'page', 'facilities'));
            } elseif($page->template == 'accomodation') {
                return view('frontend/pages/accomodation',compact('slides', 'page', 'accomodations'));
            } elseif($page->template == 'facility') {
                return view('frontend/pages/facilities',compact('slides', 'page', 'facilities', 'feat_facility', 'other_facilities'));
            } elseif($page->template == 'contact') {
                return view('frontend/pages/contact',compact('slides', 'page'));
            } elseif($page->template == 'gallery') {
                return view('frontend/pages/gallery',compact('slides', 'page'));
            } else {
                return view('frontend/pages/page',compact('slides', 'page'));
            }
        }else{
            abort(404);
        }
    }
    
    public function accomodationDetail($slug) 
    {
        $accomodation  = Accomodation::where('slug', $slug)->active()->first();
        if($accomodation) {
            if($accomodation->slider == 1 && $accomodation->slider_identifier != '')
            {
               $slides = Slide::where('group_identifier', $accomodation->slider_identifier)->order()->get(); 
            } else {
                $slides = array();
            }
        } else {
            abort(404);
        }
        return view('frontend/pages/accomodation-detail', compact('accomodation', 'slides'));
    }

    public function facilitiesDetail($slug) 
    {
        $facility           = Facility::where('slug', $slug)->detailpage()->active()->first();
        
        if($facility) {
            $other_facilities   = Facility::select('id', 'slug', 'title', 'feat_img')->active()->order()->detailpage()->where('id', '!=', $facility->id)->get();
            if($facility->slider == 1 && $facility->slider_identifier != '')
            {
                $slides = Slide::where('group_identifier', $facility->slider_identifier)->order()->get(); 
            } else {
                $slides = array();
            }
        } else {
           abort(404); 
        }
        return view('frontend/pages/facilities-detail', compact('facility', 'slides', 'other_facilities'));
    }


    public function sendEmail(Request $request){
            $params = array();
            parse_str($_POST['values'],$params);
            $from = $params['emailadd']; 
            $user = $params['username'];
            $subject = $params['subject'];
            $content = $params['message'];
            $setting = GeneralSetting::select('email','logo')->first();
            $to = $setting->email;
            Mail::send('frontend.email_template.contactmail', ['user' => $user,'email' => $from, 'subject' => $subject, 'content' => $content,'setting' => $setting], function ($m) use ($to, $subject,$from) {
                $m->from('noreply@hotelpokhara-goodwill.com', 'From Website: '.$subject);
                $m->to( $to )->subject($subject);
            });
            return response()->json(['stat'=> 'success']);
            
    }

}

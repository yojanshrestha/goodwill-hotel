<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Testimonial\CreateTestimonialRequest;
use App\Http\Requests\Backend\Testimonial\DeleteTestimonialRequest;
use App\Http\Requests\Backend\Testimonial\UpdateTestimonialRequest;
use App\Models\Testimonial;
use DB;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


/**
 * Class TestimonialController.
 */
class TestimonialController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.testimonials.index');
    }

    public function load(){
        $testimonials = Testimonial::select('id', 'name', 'address', 'ordering', 'status', 'created_at', 'updated_at');
        return Datatables::of($testimonials)
            ->escapeColumns(['name', 'address', 'ordering'])
            ->addColumn('bulk', function ($data) {
                return bulkSelect($data->id);
            })
            ->editColumn('created_at', function($data){ 
                return parseDateTimeY_M_D($data->created_at) ;
            })
            ->editColumn('updated_at', function($data){ 
                return parseDateTimeY_M_D($data->updated_at) ;
            })
            ->editColumn('status', function ($data) {
                return $data->status_label;
            })
            ->addColumn('action', function($data){
                return crudOps('testimonials', $data->id);
            })
            ->make(true);
    }

    public function create(CreateTestimonialRequest $request){
    	return view('backend.testimonials.create');
    }

    public function store(CreateTestimonialRequest $request){
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'review' => 'required',
            'ordering' => 'required',
            'status' => 'required',
        ]);
        DB::transaction(function () use ($request) {
            Testimonial::create([
                'name' => $request->name,    
                'address' => $request->address,    
                'review' => $request->review,    
                'ordering' => $request->ordering,    
                'status' => $request->status,    
            ]);
        });

        return redirect()->route('admin.testimonials.index')->withFlashSuccess('Testimonial created successfully.');
    }

    public function edit($id, UpdateTestimonialRequest $request){
        $testimonial = Testimonial::findOrFail($id);
        return view('backend.testimonials.edit',compact('testimonial'));
    }

    public function update($id, UpdateTestimonialRequest $request){
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'review' => 'required',
            'ordering' => 'required',
            'status' => 'required',
        ]);
        $testimonial = Testimonial::findOrFail($id);
        DB::transaction(function () use ($request, $testimonial) {
            $testimonial->update([
                'name' => $request->name,    
                'address' => $request->address,    
                'review' => $request->review,    
                'ordering' => $request->ordering,    
                'status' => $request->status,   
            ]);
        });
        return redirect()->route('admin.testimonials.index')->withFlashSuccess('Testimonial updated successfully.');
    }

    public function destroy($id, DeleteTestimonialRequest $request){
        DB::transaction(function () use ($id) {
            Testimonial::destroy($id);
        });
        return redirect()->back()->withFlashSuccess('Testimonial deleted successfully.');
    }


    /**
     * Remove bulk testimonials from storage.
     *
     * @param  App\Http\Requests\Backend\Product\DeleteTestimonialRequest $request
     * @return \Illuminate\Http\Response
     */
    public function deleteTestimonials(DeleteTestimonialRequest $request)
    {
        if (empty($request->ids)) {
            return redirect('/admin/testimonials')->withFlashDanger('Please select testimonials to delete.');
        }

        DB::transaction(function() use ($request){
            $ids = explode(',', $request->ids);
            foreach ($ids as $key => $id) {
                $product = Testimonial::findOrFail($id);
                $product->delete();
            }
        });
        return redirect('/admin/testimonials')->withFlashSuccess('Testimonials deleted successfully.');
    }
    

}

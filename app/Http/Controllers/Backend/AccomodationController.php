<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Accomodation\CreateAccomodationRequest;
use App\Http\Requests\Backend\Accomodation\DeleteAccomodationRequest;
use App\Http\Requests\Backend\Accomodation\UpdateAccomodationRequest;
use App\Models\Accomodation;
use App\Models\Facility;
use App\Models\Slide;
use DB;
use Datatables;
use File;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class AccomodationController.
 */
class AccomodationController extends Controller
{
    protected $destination_path = 'images/accomodations';
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.accomodations.index');
    }

    public function load(){
        $accomodations = Accomodation::select('id', 'title', 'slug', 'ordering', 'status', 'created_at', 'updated_at');
        return Datatables::of($accomodations)
            ->escapeColumns(['title', 'slug', 'ordering'])
            ->addColumn('bulk', function ($data) {
                return bulkSelect($data->id);
            })
            ->editColumn('created_at', function($data){ 
                return parseDateTimeY_M_D($data->created_at);
            })
            ->editColumn('updated_at', function($data){ 
                return parseDateTimeY_M_D($data->updated_at);
            })
            ->editColumn('status', function ($data) {
                return $data->status_label;
            })
            ->addColumn('action', function($data){
                return crudOps('accomodations', $data->id);
            })
            ->make(true);
    }

    public function create(CreateAccomodationRequest $request){
        $facilities = Facility::active()->order()->get();
        $sliders = Slide::select('title', 'group_identifier')->groupBy('title', 'group_identifier')->get();
        $sliders = $sliders->pluck('title', 'group_identifier');
        $sliders->prepend('-- Select Slider --', '');
        return view('backend.accomodations.create', compact('facilities', 'sliders'));
    }

    public function store(CreateAccomodationRequest $request){
        $this->validate($request,[
            'title' => 'required',
            'slug' => 'unique:accomodations,slug',
            'status' => 'required',
            'feat_img' => 'image',
            // 'feature_title.*' => 'required',
            'gallery_ordering.*' => 'required',
            'gallery.*' => 'required|image',
        ]);
        DB::transaction(function () use ($request) {
            if (empty($request->slug)) {
               $slug = generateUniqueSlug('App\Models\Accomodation', $request->title);
            }else{
                $slug = $request->slug;
            }

            $featImageName = '';
            if ($request->hasFile('feat_img')) {
                $file= $request->file('feat_img');
                $featImageName = str_random(5).'-'.str_replace(' ', '-', $file->getClientOriginalName());
            }

            $accomodation = Accomodation::create([
                'title' => $request->title,    
                'slug' => $slug,    
                'content' => $request->content,    
                'feat_img' => $featImageName,    
                'ordering' => (!empty($request->ordering))? $request->ordering: 0,    
                'status' => $request->status,
                'meta_title' => $request->meta_title,    
                'meta_keyword' => $request->meta_keyword,    
                'meta_desc' => $request->meta_desc,
                'slider' => $request->slider,    
                'slider_identifier' => $request->slider_identifier,    
            ]);

            if ($request->hasFile('feat_img')) {
                $file->move($this->destination_path.'/'.$accomodation->id,$featImageName);
            }

            if(count($request->facility_id) > 0){
                $accomodation->facilities()->attach($request->facility_id);
            }

            $i = 0;
            while($i < count($request->gallery_ordering)){
                $file = $request->file('gallery');
                $path = $this->destination_path.'/'.$accomodation->id.'/sliders';

                if (!empty($file[$i])) {
                    $filename = str_random(5) . '-' . str_replace(' ', '-', $file[$i]->getClientOriginalName());
                    $file[$i]->move($path, $filename);

                    $accomodation->sliders()->create([
                        'image' => $filename,
                        'ordering' => $request->gallery_ordering[$i],
                    ]);
                }
                $i++;
            }

        });

        return redirect()->route('admin.accomodations.index')->withFlashSuccess('Accomodation created successfully.');
    }

    public function edit($id, UpdateAccomodationRequest $request){
        $accomodation = Accomodation::findOrFail($id);
        $facilities = Facility::active()->order()->get();
        $selectedFacilities = $accomodation->facilities()->order()->get()->pluck('id')->toArray();
         $sliders = Slide::select('title', 'group_identifier')->groupBy('title', 'group_identifier')->get();
        $sliders = $sliders->pluck('title', 'group_identifier');
        $sliders->prepend('-- Select Slider --', '');
        return view('backend.accomodations.edit',compact('accomodation', 'facilities', 'selectedFacilities', 'sliders'));
    }

    public function update($id, UpdateAccomodationRequest $request){
        $this->validate($request,[
            'title' => 'required',
            'slug' => 'unique:accomodations,slug,'.$id,
            'status' => 'required',
            // 'feat_img' => 'image',
            // 'feature_title.*' => 'required',
            'gallery_ordering.*' => 'required',
            // 'gallery.*' => 'required|image',
        ]);
        $accomodation = Accomodation::findOrFail($id);
        DB::transaction(function () use ($request, $accomodation) {

            if (empty($request->slug)) {
               $slug = generateUniqueSlug('App\Models\Accomodation', $request->title, $accomodation->id);
            }else{
                $slug = $request->slug;
            }

            $featImageName = $accomodation->feat_img;
            if ($request->hasFile('feat_img')) {
                $file= $request->file('feat_img');
                $featImageName = str_random(5).'-'.str_replace(' ', '-', $file->getClientOriginalName());
                $file->move($this->destination_path.'/'.$accomodation->id,$featImageName);
                
                if (!empty($accomodation->feat_img)) {
                    if (File::exists($this->destination_path.'/'.$accomodation->id.'/'.$accomodation->feat_img)) {
                      unlink($this->destination_path.'/'.$accomodation->id.'/'.$accomodation->feat_img);
                    }
                }            
            }

            $accomodation->update([
                'title' => $request->title,    
                'slug' => $slug,    
                'content' => $request->content,  
                'feat_img' => $featImageName,    
                'ordering' => (!empty($request->ordering))? $request->ordering: 0,    
                'status' => $request->status,
                'meta_title' => $request->meta_title,    
                'meta_keyword' => $request->meta_keyword,    
                'meta_desc' => $request->meta_desc,
                'slider' => $request->slider,   
                'slider_identifier' => $request->slider_identifier,
            ]);

            if (count($request->facility_id) > 0) {
                $accomodation->facilities()->sync($request->facility_id);
            }else{
                $accomodation->facilities()->sync([]);
            }

            $prevGallery = $accomodation->sliders()->orderBy('ordering')->get();
            $prevGalleryCount = count($prevGallery);
            $galleryCount = count($request->gallery_ordering);
            $file = $request->file('gallery');
            $path = $this->destination_path.'/'.$accomodation->id.'/sliders';
            $i = 0;
            if ($prevGalleryCount <= $galleryCount) {
                while ($i < $prevGalleryCount) {
                    if (!empty($file[$i])) {
                        $filename = str_random(5) . '-' . str_replace(' ', '-', $file[$i]->getClientOriginalName());
                        $move = $file[$i]->move($path, $filename);
                        
                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $filename,
                        ]);
                    }else{
                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $request->prev_image[$i],
                        ]);
                    }
                    $i++;
                }
                while ($i < $galleryCount) {
                    if (!empty($file[$i])) {
                        $filename = str_random(5) . '-' . $file[$i]->getClientOriginalName();
                        $file[$i]->move($path, $filename);

                        $accomodation->sliders()->create([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $filename,
                        ]);
                    }
                    $i++;
                }
            }else{
                while ($i < $galleryCount) {
                    if (!empty($file[$i])) {
                        $filename = str_random(5) . '-' . $file[$i]->getClientOriginalName();
                        $move = $file[$i]->move($path, $filename);

                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $filename,
                        ]);
                    }else{
                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $request->prev_image[$i],
                        ]);
                    }
                    $i++;
                }
                while ($i < $prevGalleryCount) {
                    $prevGallery[$i]->delete();
                    $i++;
                }
            }

            $gallerysArray = $accomodation->sliders()->select('image')->get()->pluck('image')->toArray();

            $dir = public_path().'/'.$path;
            if (file_exists($dir) == false) {
                echo 'Directory \''. $dir. '\' not found!';
            } else {
                $dir_contents = scandir($dir);
                foreach ($dir_contents as $file) {
                    if ($file !== '.' && $file !== '..') {
                        if (!(in_array($file, $gallerysArray))) {
                            deleteFile($path.'/'.$file);
                        }
                    }
                }
            }

        });

        return redirect()->route('admin.accomodations.index')->withFlashSuccess('Accomodation updated successfully.');
    }

    public function destroy($id, DeleteAccomodationRequest $request){
        DB::transaction(function () use ($id) {
            $accomodation = Accomodation::findOrFail($id);
            $id = $accomodation->id;
            $accomodation->delete();
            delete_files($this->destination_path.'/'.$id.'/');
        });
        return redirect()->back()->withFlashSuccess('Accomodation deleted successfully.');
    }


    /**
     * Remove bulk accomodations from storage.
     *
     * @param  App\Http\Requests\Backend\Product\DeleteAccomodationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function deleteAccomodations(DeleteAccomodationRequest $request)
    {
        if (empty($request->ids)) {
            return redirect('/admin/accomodations')->withFlashDanger('Please select accomodations to delete.');
        }

        DB::transaction(function() use ($request){
            $ids = explode(',', $request->ids);
            foreach ($ids as $key => $id) {
                $accomodation = Accomodation::findOrFail($id);
                $accomodation->delete();
                delete_files($this->destination_path.'/'.$id.'/');
            }
        });
        return redirect('/admin/accomodations')->withFlashSuccess('Accomodations deleted successfully.');
    }

}

<?php
namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Request;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Category;
use View;
use DB;
use Redirect;
use Validator;
use App\Http\Requests\Backend\Category\CreateCategoryRequest;
use App\Http\Requests\Backend\Category\DeleteCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;

class CategoryController extends Controller {

	protected $layout = 'layout';

	public function getIndex()
	{	
		$items 	= Category::orderBy('order')->get();

		$menu 	= new Category;
		$menu   = $menu->getHTML($items);

		// $this->layout->content = View::make('admin.menu.builder', array('items'=>$items,'menu'=>$menu));
		return view('backend.category.builder', compact('items', 'menu'));  //i have used this instead of above;
	}

	public function getNew(){
		return view('backend.category.create');
	}

	public function getEdit($id, UpdateCategoryRequest $request)
	{	
		$item = Category::find($id);
		// $this->layout->content = View::make('admin.menu.edit', array('item'=>$item));
		return view('backend.category.edit', compact('item'));  //i have used this instead of above;

	}

	public function postEdit($id, UpdateCategoryRequest $request)
	{	
		$this->validate($request,[
            // 'title' => 'required|unique:categorys,title,'.$id,
            'title' => 'required',
            'status' => 'required',
            'url' => 'unique:categorys,url,'.$id,
        ]);

		DB::transaction(function () use ($request, $id) {
			$item = Category::find($id);

	        if (empty($request->url)) {
	           $url = generateUniqueUrl('App\Models\Category', $request->title, $item->id);
	        }else{
	            $url = $request->url;
	        }

			// $item->title 	= e(Request::get('title','untitled'));
			// $item->label 	= e(Request::get('label',''));	
			// $item->url 		= e(Request::get('url',''));	

			// $item->save();

			$item->title 	= Request::get('title','untitled');
			$item->label 	= Request::get('label','');	
			$item->url 		= $url;	
			$item->status 		= Request::get('status','');	
			$item->save();

		});
		return redirect('admin/menu')->withFlashSuccess('Menu updated successfully.');
	}

	// AJAX Reordering function
	public function postIndex()
	{	
	    $source       = e(Request::get('source'));
	    $destination  = e(Request::get('destination', 0));

	    $item             = Category::find($source);

	    if(empty($destination)){
	    	$destination = 0;
	    }
	    $item->parent_id  = $destination;
	    $item->save();

	    $ordering       = json_decode(Request::get('order'));
	    $rootOrdering   = json_decode(Request::get('rootOrder'));

	    if($ordering){
	      foreach($ordering as $order=>$item_id){
	        if($itemToOrder = Category::find($item_id)){
	            $itemToOrder->order = $order;
	            $itemToOrder->save();
	        }
	      }
	    } else {
	      foreach($rootOrdering as $order=>$item_id){
	        if($itemToOrder = Category::find($item_id)){
	            $itemToOrder->order = $order;
	            $itemToOrder->save();
	        }
	      }
	    }

	    return 'ok ';
	}

	public function postNew(CreateCategoryRequest $request)
	{	
		$this->validate($request,[
            // 'title' => 'required|unique:categorys,title',
            'title' => 'required',
            'status' => 'required',
            'url' => 'unique:categorys,url',
        ]);

		DB::transaction(function () use ($request) {
	        if (empty($request->url)) {
	           $url = generateUniqueUrl('App\Models\Category', $request->title);
	        }else{
	            $url = $request->url;
	        }

			// Create a new menu item and save it
			$item = new Category;

			$item->title 	= Request::get('title','untitled');
			$item->label 	= Request::get('label','');	
			$item->url 		= $url;	
			$item->order 	= Category::max('order')+1;
			$item->status 		= Request::get('status','');	
			$item->save();

		});

		return Redirect::to('admin/menu');
	}

	public function postDelete(DeleteCategoryRequest $request)
	{
		$id = Request::get('delete_id');
		// Find all items with the parent_id of this one and reset the parent_id to zero
		$items = Category::where('parent_id', $id)->get()->each(function($item)
		{
			$item->parent_id = 0;  
			$item->save();  
		});

		// Find and delete the item that the user requested to be deleted
		$item = Category::find($id);
		$item->delete();

		return Redirect::to('admin/menu');
	}
}
<?php

namespace App\Http\Controllers\Backend;

use DB;
use File;
use Datatables;
use App\Models\Block;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests\Backend\Block\CreateBlockRequest;
use App\Http\Requests\Backend\Block\EditBlockRequest;
use App\Http\Requests\Backend\Block\DeleteBlockRequest;

class BlockController extends Controller
{

    protected $baseDir="images/blocks";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //return view('backend.blocks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $blocks = Block::order()->get();
        if(count($blocks) > 0) {
            return view('backend.blocks.edit', compact('blocks'));
        } else {            
            return view('backend.blocks.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlockRequest $request)
    { 
            $blocks = Block::order()->get();
            if (count($blocks) > 0) {
                $msg = "Blocks Updated Successfully.";
            } else {
                $msg = "Blocks Created Successfully.";
            }

            if (count($blocks) > 0) {
                if(count($request->title) > 0){
                    $files          = $_FILES['feat_img']['tmp_name'];
                    $images         = $_FILES['feat_img']['name'];
                }
                $dbcount        = count($blocks);
                $bladecount     = count($request->title);
                $i              = 0;
                if ($dbcount <= $bladecount) {
                    while ($i < $dbcount) { 
                        if (!empty($files[$i])) {
                            $imagename  = str_replace(' ', '-', $images[$i]);
                            $filepath   = 'images/blocks/'.time().$imagename;
                            move_uploaded_file($files[$i], $filepath);
                            $oldfile    = $request->pre_image[$i];
                            if (File::exists($oldfile)) { // unlink or remove previous image from folder
                                unlink($oldfile);
                            }
                        } else {
                            $filepath   = $request->pre_image[$i];
                        }
                        $blocks[$i]->update([
                            'title'    => $request->title[$i],
                            'slug'    => $request->slug[$i],
                            'ordering' => (!empty($request->ordering[$i]))? $request->ordering[$i]: 0,
                            'description'    => $request->description[$i],
                            'image'               => $filepath,
                            'video'           => $request->video[$i],
                            ]);
                        $i++;
                    } 
                    while ($i < $bladecount) { 
                        if (!empty($files[$i])) {
                            $imagename  = str_replace(' ', '-', $images[$i]);
                            $filepath   = 'images/blocks/'.time().$imagename;
                            move_uploaded_file($files[$i], $filepath);
                        } else {
                            $filepath   = '';
                        }
                        $ad = Block::create([
                            'title'    => $request->title[$i],
                            'slug'    => $request->slug[$i],
                            'ordering' => (!empty($request->ordering[$i]))? $request->ordering[$i]: 0,
                            'description'    => $request->description[$i],
                            'image'               => $filepath,
                            'video'           => $request->video[$i],
                            ]);
                        $i++;
                    }
                } else {
                    while ($i < $bladecount) {
                        if (!empty($files[$i])) {
                            $imagename  = str_replace(' ', '-', $images[$i]);
                            $filepath   = 'images/blocks/'.time().$imagename;
                            move_uploaded_file($files[$i], $filepath);
                            $oldfile    = $request->pre_image[$i];
                            if (File::exists($oldfile)) { // unlink or remove previous image from folder
                                unlink($oldfile);
                            }
                        } else {
                            $filepath   = $request->pre_image[$i];
                        }
                        $blocks[$i]->update([
                            'title'    => $request->title[$i],
                            'slug'    => $request->slug[$i],
                            'ordering' => (!empty($request->ordering[$i]))? $request->ordering[$i]: 0,
                            'description'    => $request->description[$i],
                            'image'               => $filepath,
                            'video'           => $request->video[$i],
                            ]);
                        $i++;
                    } 
                    while ($i < $dbcount) {
                        $oldfile        = $blocks[$i]->blocks;
                        $blocks[$i]->delete();
                        $oldfilerow     = Block::where('image', $oldfile)->first();
                        if (empty($oldfilerow)) { 
                            if (File::exists($oldfile)) { // unlink or remove previous image from folder
                            unlink($oldfile);
                            }
                        }
                        $i++;
                    }
                }
            } else { 
                    $files          = $_FILES['feat_img']['tmp_name'];
                    $images         = $_FILES['feat_img']['name'];
                foreach ($request->title as $key => $title) {
                    if (!empty($files[$key])) {
                        $imagename  = str_replace(' ', '-', $images[$key]);
                        $filepath   = 'images/blocks/'.time().$imagename;
                        move_uploaded_file($files[$key], $filepath);
                    } else {
                        $filepath   = '';
                    }
                    $block = Block::create([
                        'title'    => $title,
                        'slug'    => $request->slug[$key],
                        'ordering' => (!empty($request->ordering[$key]))? $request->ordering[$key]: 0,
                        'description'    => $request->description[$key],
                        'image'               => $filepath,
                        'video'           => $request->video[$key],
                    ]);
                }
            }
            // delete images in upload folder which is not in database
            $imagesArray        = Block::select('image')->get()->pluck('image')->toArray();
            $destination_path   = 'images/blocks/';
            $dir = public_path().'/images/blocks/';

            if (file_exists($dir) == false) {
                echo 'Directory \''. $dir. '\' not found!';
            } else {
                $dir_contents = scandir($dir);
                foreach ($dir_contents as $dir_content) {
                    if ($dir_content !== '.' && $dir_content !== '..') {
                        if (!(in_array($destination_path.$dir_content, $imagesArray))) {
                            deleteFile($destination_path.$dir_content);
                        }
                    }
                }
            }
        return redirect()->route('admin.blocks')->withFlashSuccess($msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function makeFileName(UploadedFile $file)
    {

        $name   = time().$file->getClientOriginalName();
            
        return "{$name}";

    }
        /**
     * Make a Filepath for file
     * 
     * @return String
     * 
     */
    protected function Upload_and_GetFilepath(UploadedFile $file, $filename)
    {
        $Filepath = $this->baseDir."/".$filename;

        if($file->move($this->baseDir,$filename)) {
            return $Filepath;
        } else {
            return "upload fail";
        }
    }
}

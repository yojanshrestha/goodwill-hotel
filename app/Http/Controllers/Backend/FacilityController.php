<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Facility\CreateFacilityRequest;
use App\Http\Requests\Backend\Facility\DeleteFacilityRequest;
use App\Http\Requests\Backend\Facility\UpdateFacilityRequest;
use App\Models\Facility;
use App\Models\Slide;
use DB;
use Datatables;
use File;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Class FacilityController.
 */
class FacilityController extends Controller
{
    protected $destination_path = 'images/facilities';
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.facilities.index');
    }

    public function load(){
        $facilities = Facility::select('id', 'title', 'slug', 'featured_facility', 'ordering', 'status', 'accomodation_only', 'detail_page', 'created_at', 'updated_at');
        return Datatables::of($facilities)
            ->escapeColumns(['title', 'slug', 'ordering'])
            ->addColumn('bulk', function ($data) {
                return bulkSelect($data->id);
            })
            // ->editColumn('accomodation_only', function($data){ 
            //     return $data->accomodation_only_label;
            // })
            ->editColumn('featured_facility', function($data){ 
                return $data->featured_facility_label;
            })
            ->editColumn('detail_page', function($data){ 
                return $data->detail_page_label;
            })
            ->editColumn('created_at', function($data){ 
                return parseDateTimeY_M_D($data->created_at);
            })
            ->editColumn('updated_at', function($data){ 
                return parseDateTimeY_M_D($data->updated_at);
            })
            ->editColumn('status', function ($data) {
                return $data->status_label;
            })
            ->addColumn('action', function($data){
                return crudOps('facilities', $data->id);
            })
            ->make(true);
    }

    public function create(CreateFacilityRequest $request){
        $sliders = Slide::select('title', 'group_identifier')->groupBy('title', 'group_identifier')->get();
        $sliders = $sliders->pluck('title', 'group_identifier');
        $sliders->prepend('-- Select Slider --', '');
        return view('backend.facilities.create', compact('sliders'));
    }

    public function store(CreateFacilityRequest $request){
        $this->validate($request,[
            'title' => 'required',
            'slug' => 'unique:facilities,slug',
            'status' => 'required',
            'feat_img' => 'image',
            'feature_title.*' => 'required',
            'gallery_ordering.*' => 'required',
            'gallery.*' => 'required|image',
        ]);
        DB::transaction(function () use ($request) {
            if (empty($request->slug)) {
               $slug = generateUniqueSlug('App\Models\Facility', $request->title);
            }else{
                $slug = $request->slug;
            }

            $featImageName = '';
            if ($request->hasFile('feat_img')) {
                $file= $request->file('feat_img');
                $featImageName = str_random(5).'-'.str_replace(' ', '-', $file->getClientOriginalName());
            }

            $facility = Facility::create([
                'title' => $request->title,    
                'slug' => $slug,    
                'content' => $request->content,    
                'featured_facility' => (!empty($request->featured_facility))? $request->featured_facility: 0,    
                'feat_img' => $featImageName,    
                'ordering' => (!empty($request->ordering))? $request->ordering: 0,    
                'accomodation_only' => (!empty($request->accomodation_only))? $request->accomodation_only: 0,    
                'detail_page' => (!empty($request->detail_page))? $request->detail_page: 0,    
                'about_page' => (!empty($request->about_page))? $request->about_page: 0,    
                'status' => $request->status,    
                'meta_title' => $request->meta_title,    
                'meta_keyword' => $request->meta_keyword,    
                'meta_desc' => $request->meta_desc,
                'slider' => $request->slider,    
                'slider_identifier' => $request->slider_identifier,   
            ]);

            if ($request->hasFile('feat_img')) {
                $file->move($this->destination_path.'/'.$facility->id,$featImageName);
            }

            if(count($request->feature_title) > 0){
                foreach ($request->feature_title as $key => $featTitle) {
                    $facility->features()->create([
                        'title' => $featTitle,
                    ]);
                }
            }

            $i = 0;
            while($i < count($request->gallery_ordering)){
                $file = $request->file('gallery');
                $path = $this->destination_path.'/'.$facility->id.'/sliders';

                // if ($request->hasFile('gallery')) {
                if (!empty($file[$i])) {
                    $filename = str_random(5) . '-' . str_replace(' ', '-', $file[$i]->getClientOriginalName());
                    $file[$i]->move($path, $filename);

                    $facility->sliders()->create([
                        'image' => $filename,
                        'ordering' => $request->gallery_ordering[$i],
                    ]);
                }
                $i++;
            }

        });

        return redirect()->route('admin.facilities.index')->withFlashSuccess('Facility created successfully.');
    }

    public function edit($id, UpdateFacilityRequest $request){
        $facility = Facility::findOrFail($id);
        $sliders = Slide::select('title', 'group_identifier')->groupBy('title', 'group_identifier')->get();
        $sliders = $sliders->pluck('title', 'group_identifier');
        $sliders->prepend('-- Select Slider --', '');
        return view('backend.facilities.edit',compact('facility', 'sliders'));
    }

    public function update($id, UpdateFacilityRequest $request){
        $this->validate($request,[
            'title' => 'required',
            'slug' => 'unique:facilities,slug,'.$id,
            'status' => 'required',
            // 'feat_img' => 'image',
            'feature_title.*' => 'required',
            'gallery_ordering.*' => 'required',
            // 'gallery.*' => 'required|image',
        ]);
        $facility = Facility::findOrFail($id);
        DB::transaction(function () use ($request, $facility) {

            if (empty($request->slug)) {
               $slug = generateUniqueSlug('App\Models\Facility', $request->title, $facility->id);
            }else{
                $slug = $request->slug;
            }

            $featImageName = $facility->feat_img;
            if ($request->hasFile('feat_img')) {
                $file= $request->file('feat_img');
                $featImageName = str_random(5).'-'.str_replace(' ', '-', $file->getClientOriginalName());
                $file->move($this->destination_path.'/'.$facility->id,$featImageName);
                
                if (!empty($facility->feat_img)) {
                    if (File::exists($this->destination_path.'/'.$facility->id.'/'.$facility->feat_img)) {
                      unlink($this->destination_path.'/'.$facility->id.'/'.$facility->feat_img);
                    }
                }            
            }

            $facility->update([
                'title' => $request->title,    
                'slug' => $slug,    
                'content' => $request->content,  
                'featured_facility' => (!empty($request->featured_facility))? $request->featured_facility: 0,  
                'feat_img' => $featImageName,    
                // 'ordering' => $request->ordering,    
                'ordering' => (!empty($request->ordering))? $request->ordering: 0,    
                'accomodation_only' => (!empty($request->accomodation_only))? $request->accomodation_only: 0,    
                'detail_page' => (!empty($request->detail_page))? $request->detail_page: 0,    
                'about_page' => (!empty($request->about_page))? $request->about_page: 0,    
                'status' => $request->status,
                'meta_title' => $request->meta_title,    
                'meta_keyword' => $request->meta_keyword,    
                'meta_desc' => $request->meta_desc,
                'slider' => $request->slider,   
                'slider_identifier' => $request->slider_identifier,
            ]);

            $prevFeat = $facility->features()->get();
            $prevFeatCount = count($prevFeat);
            $featCount = count($request->feature_title);
            $i = 0;
            if ($prevFeatCount <= $featCount) {
                while ($i < $prevFeatCount) {
                    $prevFeat[$i]->update([
                        'title' => $request->feature_title[$i],
                    ]);
                    $i++;
                }
                while ($i < $featCount) {
                    $facility->features()->create([
                        'title' => $request->feature_title[$i],
                    ]);
                    $i++;
                }
            }else{
                while ($i < $featCount) {
                    $prevFeat[$i]->update([
                        'title' => $request->feature_title[$i],
                    ]);
                    $i++;
                }
                while ($i < $prevFeatCount) {
                    $prevFeat[$i]->delete();
                    $i++;
                }
            }

            $prevGallery = $facility->sliders()->orderBy('ordering')->get();
            $prevGalleryCount = count($prevGallery);
            $galleryCount = count($request->gallery_ordering);
            $file = $request->file('gallery');
            $path = $this->destination_path.'/'.$facility->id.'/sliders';
            $i = 0;
            if ($prevGalleryCount <= $galleryCount) {
                while ($i < $prevGalleryCount) {
                    if (!empty($file[$i])) {
                        $filename = str_random(5) . '-' . str_replace(' ', '-', $file[$i]->getClientOriginalName());
                        $move = $file[$i]->move($path, $filename);
                        
                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $filename,
                        ]);
                    }else{
                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $request->prev_image[$i],
                        ]);
                    }
                    $i++;
                }
                while ($i < $galleryCount) {
                    if (!empty($file[$i])) {
                        $filename = str_random(5) . '-' . $file[$i]->getClientOriginalName();
                        $file[$i]->move($path, $filename);

                        $facility->sliders()->create([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $filename,
                        ]);
                    }
                    $i++;
                }
            }else{
                while ($i < $galleryCount) {
                    if (!empty($file[$i])) {
                        $filename = str_random(5) . '-' . $file[$i]->getClientOriginalName();
                        $move = $file[$i]->move($path, $filename);

                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $filename,
                        ]);
                    }else{
                        $prevGallery[$i]->update([
                            'ordering' => $request->gallery_ordering[$i],
                            'image' => $request->prev_image[$i],
                        ]);
                    }
                    $i++;
                }
                while ($i < $prevGalleryCount) {
                    $prevGallery[$i]->delete();
                    $i++;
                }
            }

            $gallerysArray = $facility->sliders()->select('image')->get()->pluck('image')->toArray();

            $dir = public_path().'/'.$path;
            if (file_exists($dir) == false) {
                echo 'Directory \''. $dir. '\' not found!';
            } else {
                $dir_contents = scandir($dir);
                foreach ($dir_contents as $file) {
                    if ($file !== '.' && $file !== '..') {
                        if (!(in_array($file, $gallerysArray))) {
                            deleteFile($path.'/'.$file);
                        }
                    }
                }
            }

        });

        return redirect()->route('admin.facilities.index')->withFlashSuccess('Facility updated successfully.');
    }

    public function destroy($id, DeleteFacilityRequest $request){
        DB::transaction(function () use ($id) {
            $facility = Facility::findOrFail($id);
            $id = $facility->id;
            $facility->delete();
            delete_files($this->destination_path.'/'.$id.'/');
        });
        return redirect()->back()->withFlashSuccess('Facility deleted successfully.');
    }


    /**
     * Remove bulk facilities from storage.
     *
     * @param  App\Http\Requests\Backend\Product\DeleteFacilityRequest $request
     * @return \Illuminate\Http\Response
     */
    public function deleteFacilities(DeleteFacilityRequest $request)
    {
        if (empty($request->ids)) {
            return redirect('/admin/facilities')->withFlashDanger('Please select facilities to delete.');
        }

        DB::transaction(function() use ($request){
            $ids = explode(',', $request->ids);
            foreach ($ids as $key => $id) {
                $facility = Facility::findOrFail($id);
                $facility->delete();
                delete_files($this->destination_path.'/'.$id.'/');
            }
        });
        return redirect('/admin/facilities')->withFlashSuccess('Facilitys deleted successfully.');
    }

}

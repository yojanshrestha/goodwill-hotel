<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\GeneralSetting;
use App\Models\Page;
use App\Models\Slide;
use View;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $categories = Category::select('id', 'parent_id', 'title', 'url', 'status')
            ->root()
            ->active()
            ->order()
            ->with('activeChildren.activeChildren')
            ->get();

    	$settings = GeneralSetting::first();
    	if(empty($settings)){
    		$settings = new GeneralSetting;
    		$settings->title = env('APP_NAME');
    	}

        $footermenus = Page::select('id', 'title', 'slug')->where('footer_menu', 1)->orderBy('footer_ordering')->active()->get();

        // sharing data across all views
        view::share ('footermenus', $footermenus);
        view::share ('categories', $categories);
    	view::share ('settings', $settings);
    }

}

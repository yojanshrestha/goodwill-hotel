<?php namespace App\Http\Requests\Backend\Accomodation;

use App\Http\Requests\Request;

/**
 * Class UpdateAccomodationRequest
 */
class UpdateAccomodationRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return access()->allow('edit-accomodations');
	}

		/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	
}
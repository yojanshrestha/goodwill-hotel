<?php namespace App\Http\Requests\Backend\Facility;

use App\Http\Requests\Request;

/**
 * Class UpdateFacilityRequest
 */
class UpdateFacilityRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return access()->allow('edit-facilities');
	}

		/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	
}
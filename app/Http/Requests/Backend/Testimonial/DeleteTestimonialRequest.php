<?php namespace App\Http\Requests\Backend\Testimonial;

use App\Http\Requests\Request;

/**
 * Class UpdateTestimonialRequest
 */
class DeleteTestimonialRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return access()->allow('delete-testimonials');
	}

		/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	
}
<?php 
namespace App\Http\Requests\Backend\Block;

use App\Http\Requests\Request;

/**
 * Class CreateBlockRequest
 */
class CreateBlockRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return access()->allow('create-blocks');
	}

		/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//
		];
	}

	
}